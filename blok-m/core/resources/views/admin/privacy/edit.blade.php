@extends('layouts.app.frame')
@section('title', 'Edit Privacy')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('privacy.edit', $privacy))
@section('button', '<a href="'.url('/admin/privacy').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::model($privacy, [
            'method' => 'PATCH',
            'url' => ['/admin/privacy', $privacy->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}
        				
        @include ('admin.privacy.form', ['submitButtonText' => 'Update'])
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
tinymce.init({
    selector:'textarea',
    plugins: "code",
    menubar: "file edit insert view format table tools",
    height : 100
});
</script>
@endpush