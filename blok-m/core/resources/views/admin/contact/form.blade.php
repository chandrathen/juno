<div aria-required="true" class="form-group form-group-default required {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', 'Address') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('address', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', 'Phone') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('phone', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('fax') ? 'has-error' : ''}}">
    {!! Form::label('fax', 'Fax') !!}
    {!! Form::text('fax', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('fax', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('email', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('map') ? 'has-error' : ''}}">
    {!! Form::label('map', 'Map') !!}
    {!! Form::text('map', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('map', '<label class="error">:message</label>') !!}   


<div class="pull-left">
    <div class="checkbox check-success">
        <input id="checkbox-agree" type="checkbox" required> <label for="checkbox-agree">Saya sudah mengecek data sebelum menyimpan</label>
    </div>
</div>
<br/>

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<button class="btn btn-default" type="reset"><i class="pg-close"></i> Clear</button>
