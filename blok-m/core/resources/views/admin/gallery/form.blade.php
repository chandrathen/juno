</br>
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('title', '<label class="error">:message</label>') !!}
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('intro') ? 'has-error' : ''}}">
    {!! Form::label('intro', 'Intro') !!}
    {!! Form::textarea('intro', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('intro', '<label class="error">:message</label>') !!}
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('description', '<label class="error">:message</label>') !!}
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('order') ? 'has-error' : ''}}">
    {!! Form::label('order', 'Order') !!}
    {!! Form::number('order', null, ['class' => 'form-control', 'min' => '0']) !!}
</div>
{!! $errors->first('order', '<label class="error">:message</label>') !!}
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('btn_title') ? 'has-error' : ''}}">
    {!! Form::label('btn_title', 'Btn Title') !!}
    {!! Form::text('btn_title', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('btn_title', '<label class="error">:message</label>') !!}
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('link_types') ? 'has-error' : ''}}">
    {!! Form::label('link_types', 'Link Types') !!}
    {!! Form::select('link_types', ['0' => ' ', '1' => 'Internal', '2' => 'Eksternal'], null, ['class' => 'full-width', 'data-init-plugin' => 'select2']) !!}
</div>
{!! $errors->first('link_types', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group form-group-default required {{ $errors->has('link') ? 'has-error' : ''}}">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('link', '<label class="error">:message</label>') !!}

@if(isset($menu_id))
    <input type="hidden" name="menu_id" id="menu_id" value="{{ $menu_id }}">
@endif

<div class="pull-left">
    <div class="checkbox check-success">
        <input id="checkbox-agree" type="checkbox" required> <label for="checkbox-agree">Saya sudah mengecek data sebelum menyimpan</label>
    </div>
</div>
<br/>

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<button class="btn btn-default" type="reset"><i class="pg-close"></i> Clear</button>
