@extends('layouts.app.frame')
@section('title', 'Create New Collection')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('collection.new'))
@section('button', '<a href="'.url('/admin/collection').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/collection', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.collection.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush