@extends('layouts.app.frame')
@section('title', 'Edit Subscribe')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('subscribe.edit', $subscribe))
@section('button', '<a href="'.url('/admin/subscribe').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::model($subscribe, [
            'method' => 'PATCH',
            'url' => ['/admin/subscribe', $subscribe->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}
        				
        @include ('admin.subscribe.form', ['submitButtonText' => 'Update'])
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush