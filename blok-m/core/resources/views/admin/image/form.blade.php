<div aria-required="true" class="form-group form-group-default required {{ $errors->has('content_id') ? 'has-error' : ''}}">
    {!! Form::label('content_id', 'Content Id') !!}
    {!! Form::number('content_id', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('content_id', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('file_path') ? 'has-error' : ''}}">
    {!! Form::label('file_path', 'File Path') !!}
    {!! Form::text('file_path', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('file_path', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('type') ? 'has-error' : ''}}">
    {!! Form::label('type', 'Type') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('type', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('order') ? 'has-error' : ''}}">
    {!! Form::label('order', 'Order') !!}
    {!! Form::number('order', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('order', '<label class="error">:message</label>') !!}   


<div class="pull-left">
    <div class="checkbox check-success">
        <input id="checkbox-agree" type="checkbox" required> <label for="checkbox-agree">Saya sudah mengecek data sebelum menyimpan</label>
    </div>
</div>
<br/>

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<button class="btn btn-default" type="reset"><i class="pg-close"></i> Clear</button>
