@extends('layouts.app.frame')
@section('title', 'Create New Image')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('image.new'))
@section('button', '<a href="'.url('/admin/image').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/image', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.image.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush