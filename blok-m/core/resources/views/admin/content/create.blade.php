@extends('layouts.app.frame')
@section('title', 'Create New Content')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('content.new'))
@section('button', '<a href="'.url('/admin/content').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/content', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.content.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush