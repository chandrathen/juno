@extends('layouts.web.frame')

@section('content')

    <div id="slides">
        <ul class="slides-container">
            @foreach($homepage as $index => $val)
                <li><img src="{{ url('/uploads/'. $val->image) }}"></li>
            @endforeach
        </ul>
        <div id="booking" class="absolute-position book">
            <div class="booking_area right">

                <form method="post" target="_blank" action="http://juno-hotel.com/book" id="booking_form">
                    <div class="form-group title">Book Now!</div>

                    <div class="form-group" style="position:relative;">
                        <div class="row">
                            <div class="col-sm-6 paddright-5">
                                <div class="input-group date marginbott-5">
                                    <div id="datepicker-from">
                                        <font id="-from">From</font>
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 paddleft-5">
                                <div class="input-group date marginbott-5">
                                    <div id="datepicker-to" class="pull-right">
                                        <font id="-to">To</font>
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6 paddright-5">
                                <input id="guest" class="open_dropdown marginbott-5" type="text" name="guest" placeholder="Guest" readonly="readonly" sub="guest">
                                <div id="guest_list" class="dropdown" sub="guest" style="display: none;">
                                    <p for="guest">1</p>
                                    <p for="guest">2</p>
                                    <p for="guest">3</p>
                                </div>
                                <span id="guest_icon" class="icon-user-o open_dropdown" style="font-size:16px;" sub="guest"></span>
                            </div>
                            <div class="col-sm-6 paddleft-5">
                                <input id="rooms" class="open_dropdown marginbott-5" type="text" name="rooms" placeholder="Rooms" readonly="readonly" sub="rooms">
                                <div id="rooms_list" class="dropdown" sub="rooms" style="display: none;">
                                    <p for="rooms">1</p>
                                    <p for="rooms">2</p>
                                    <p for="rooms">3</p>
                                </div>
                                <span id="rooms_icon" class="icon-chevron-with-circle-right open_dropdown" style="font-size:18px;" sub="rooms"></span>
                            </div>
                        </div>
                    </div>

                    <div class="check-avail">
                        <a class="btn-avail" href="#">Check Availability</a>
                    </div>
                </form>

            </div>
        </div>

@endsection