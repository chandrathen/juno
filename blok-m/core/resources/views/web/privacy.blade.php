@extends('layouts.web.frame')

@section('content')

    <div id="content_privacy">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h1 class="title">{{ $privacy->title }}</h1>
                    {!! $privacy->description !!}
                </div>
            </div>
        </div>
    </div>

@endsection