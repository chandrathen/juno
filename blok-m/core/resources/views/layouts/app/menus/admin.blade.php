<li class="{{Request::is('admin/homepage*') ? 'active' : '' }}">
    <div class="lang-swicth">
        <a href="#" id="changes" class="lang-eng" onclick="return chng('eng')"><i class="fa fa-circle{{Session::get('lang')=='eng' ? '' : '-o'}} text-aqua" ></i> Eng</a>
        |
        <a href="#" id="changes" class="lang-ind" onclick="return chng('ind')"><i class="fa fa-circle{{Session::get('lang')=='ind' ? '' : '-o'}} text-aqua" ></i> Ind</a>
    </div>
</li>

<div class="line-separator"></div>

@if(Auth::user()->role==1)
<li class="m-t-30 {{ (Request::is('admin/user*')) ? 'active' : '' }}">
    <a href="{{url('admin/user')}}"><span class="title">User</span></a> <span class="icon-thumbnail {{ (Request::is('admin/user*')) ? 'bg-success' : '' }}"><i class="fa fa-group"></i></span>
</li>
@endif


<li class="{{Request::is('admin/homepage*') ? 'active' : '' }}">
    <a href="{{url('admin/homepage')}}"><span class="title">Homepage</span></a> <span class="icon-thumbnail {{Request::is('admin/homepage*') ? 'bg-success' : '' }}">Hp</span>
</li>

<li class="{{Request::is('admin/room*') ? 'active' : '' }}">
    <a href="{{url('admin/room')}}"><span class="title">Room</span></a> <span class="icon-thumbnail {{Request::is('admin/room*') ? 'bg-success' : '' }}">Ro</span>
</li>

<li class="{{Request::is('admin/meeting*') ? 'active' : '' }}">
    <a href="{{url('admin/meeting')}}"><span class="title">Meeting</span></a> <span class="icon-thumbnail {{Request::is('admin/meeting*') ? 'bg-success' : '' }}">Me</span>
</li>

<li class="{{Request::is('admin/facilities*') ? 'active' : '' }}">
    <a href="{{url('admin/facilities')}}"><span class="title">Facilities</span></a> <span class="icon-thumbnail {{Request::is('admin/facilities*') ? 'bg-success' : '' }}">Fa</span>
</li>

<li class="{{Request::is('admin/gallery*') ? 'active' : '' }}">
    <a href="{{url('admin/gallery')}}"><span class="title">Gallery</span></a> <span class="icon-thumbnail {{Request::is('admin/gallery*') ? 'bg-success' : '' }}">Ga</span>
</li>

<li class="{{Request::is('admin/neighbourhood*') ? 'active' : '' }}">
    <a href="{{url('admin/neighbourhood')}}"><span class="title">Neighbourhood</span></a> <span class="icon-thumbnail {{Request::is('admin/neighbourhood*') ? 'bg-success' : '' }}">Ne</span>
</li>

<!--
<li class="{{Request::is('admin/whatson*') ? 'active' : '' }}">
    <a href="{{url('admin/whatson')}}"><span class="title">Whatson</span></a><span class="icon-thumbnail {{Request::is('admin/whatson*') ? 'bg-success' : '' }}">Wh</span>
</li>

<li class="{{Request::is('admin/event*') ? 'active' : '' }}">
    <a href="{{url('admin/event')}}"><span class="whatson-sub"><i class="fa fa-minus" aria-hidden="true"></i></span> <span class="title">Event</span></a> <span class="icon-thumbnail {{Request::is('admin/event*') ? 'bg-success' : '' }}">Ev</span>
</li>

<li class="{{Request::is('admin/happening*') ? 'active' : '' }}">
    <a href="{{url('admin/happening')}}"><span class="whatson-sub"><i class="fa fa-minus" aria-hidden="true"></i></span> <span class="title">Happening</span></a><span class="icon-thumbnail {{Request::is('admin/happening*') ? 'bg-success' : '' }}">Ha</span>
</li>
-->

<li class="{{ (Request::is('admin/event*') || Request::is('admin/happening*')) ? 'open active' : ''}}">
    <a href="javascript:;"><span class="title">Whatson</span>
        <span class="arrow {{ (Request::is('admin/event*') || Request::is('admin/happening*')) ? 'open active' : ''}}"></span>
    </a>
    <span class="{{ (Request::is('admin/event*') || Request::is('admin/happening*')) ? 'bg-success' : ''}} icon-thumbnail">Wh</span>

    <ul class="sub-menu">
        <li class="{{ (Request::is('admin/event*')) ? 'active' : '' }}">
            <a href="{{url('admin/event')}}">Event</a>
            <span class="{{ (Request::is('admin/event*')) ? 'bg-success' : '' }} icon-thumbnail">Ev</span>
        </li>
        <li class="{{ (Request::is('admin/happening*')) ? 'active' : '' }}">
            <a href="{{url('admin/happening')}}">Happening</a>
            <span class="{{ (Request::is('admin/happening*')) ? 'bg-success' : '' }} icon-thumbnail">Ha</span>
        </li>
    </ul>
</li>

<div class="line-separator"></div>
{{--STATIC PAGE--}}
<li class="{{ (Request::is('admin/privacy*') || Request::is('admin/terms*')) ? 'open active' : ''}}">
    <a href="javascript:;"><span class="title">Static Page</span>
        <span class="arrow {{ (Request::is('admin/privacy*') || Request::is('admin/terms*')) ? 'open active' : ''}}"></span>
    </a>
    <span class="{{ (Request::is('admin/privacy*') || Request::is('admin/terms*')) ? 'bg-success' : ''}} icon-thumbnail">
        <i class="fa fa-briefcase"></i>
    </span>

    <ul class="sub-menu">
        <li class="{{ (Request::is('admin/privacy*')) ? 'active' : '' }}">
            <a href="{{url('admin/privacy')}}">Privacy Policy</a>
            <span class="{{ (Request::is('admin/privacy*')) ? 'bg-success' : '' }} icon-thumbnail">Pp</span>
        </li>
        <li class="{{ (Request::is('admin/terms*')) ? 'active' : '' }}">
            <a href="{{url('admin/terms')}}">Term Condition</a>
            <span class="{{ (Request::is('admin/terms*')) ? 'bg-success' : '' }} icon-thumbnail">Tc</span>
        </li>
    </ul>
</li>

{{--SOCIAL MEDIA--}}
<li class="{{Request::is('admin/socialmedia*') ? 'active' : '' }}">
    <a href="{{url('admin/socialmedia')}}"><span class="title">Social Media</span></a> <span class="icon-thumbnail {{Request::is('admin/socialmedia*') ? 'bg-success' : '' }}"><i class="fa fa-envelope"></i></span>
</li>

{{--CONTACT--}}
<li class="{{Request::is('admin/contact*') ? 'active' : '' }}">
    <a href="{{url('admin/contact')}}"><span class="title">Contact</span></a> <span class="icon-thumbnail {{Request::is('admin/contact*') ? 'bg-success' : '' }}"><i class="fa fa-envelope"></i></span>
</li>

{{--COLLECTIONS--}}
<li class="{{Request::is('admin/collection*') ? 'active' : '' }}">
    <a href="{{url('admin/collection')}}"><span class="title">Collection</span></a> <span class="icon-thumbnail {{Request::is('admin/collection*') ? 'bg-success' : '' }}"><i class="fa fa-envelope"></i></span>
</li>

{{--SUBSCRIPTION--}}
<li class="{{Request::is('admin/subscribe*') ? 'active' : '' }}">
    <a href="{{url('admin/subscribe')}}"><span class="title">Subscribe</span></a> <span class="icon-thumbnail {{Request::is('admin/subscribe*') ? 'bg-success' : '' }}"><i class="fa fa-envelope"></i></span>
</li>

@push('script')
<script type="text/javascript">
   function chng(lang){
        $.ajax({
            type: "GET",
            url: "{{url('changelang/')}}",
            data: { lan:lang },
            success: function () {
                location.reload();
            }
        });
    }
</script>
@endpush