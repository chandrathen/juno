<div id="bottom" class="hidden-md hidden-sm hidden-xs">
    <div class="container-fluid pad-0">
        <!-- For Desktop and Tablet -->
        <div class="row">

            <!-- Other properties -->
            <div class="col-md-2">
                <div class="bottom_body">
                    @if(Request::segment(1) == 'en')
                        <a href="{{ url('/en/privacy-policy') }}">PRIVACY POLICY</a>
                    @elseif(Request::segment(1) == 'id')
                        <a href="{{ url('/id/privacy-policy') }}">PRIVACY POLICY</a>
                    @endif
                    <br/>
                    @if(Request::segment(1) == 'en')
                        <a href="{{ url('/en/terms-conditions') }}">TERMS & CONDITIONS</a>
                    @elseif(Request::segment(1) == 'id')
                        <a href="{{ url('/id/terms-conditions') }}">TERMS & CONDITIONS</a>
                    @endif
                    <div class="copy-right"><p>@2017 Juno Hotel<br/>All rights reserved</p></div>
                </div>
            </div>

            <!-- Newsletter -->
            <div class="col-md-4 border bottom-height">
                <div class="bottom_body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="bottom_title">COLLECTION</div>
                            @if(Request::segment(1) == 'en')
                                @foreach($collection as $item => $val)
                                    @if($count_en <= 3)
                                        <p><a href="{{ $val->value }}" class="link-bottom" target="_blank">{{ $val->name }}</a></p>
                                    @elseif($count_en == 4)
                                        <p class="collection-4"><a href="{{ $val->value }}" class="link-bottom" target="_blank">{{ $val->name }}</a></p>
                                    @elseif($count_en == 5)
                                        <p class="collection-5"><a href="{{ $val->value }}" class="link-bottom" target="_blank">{{ $val->name }}</a></p>
                                    @endif
                                @endforeach
                            @elseif(Request::segment(1) == 'id')
                                @foreach($collection_id as $index => $data)
                                    @if($count_id <= 3)
                                        <p><a href="{{ $data->value }}" class="link-bottom" target="_blank">{{ $data->name }}</a></p>
                                    @elseif($count_id == 4)
                                        <p class="collection-4"><a href="{{ $data->value }}" class="link-bottom" target="_blank">{{ $data->name }}</a></p>
                                    @elseif($count_id == 5)
                                        <p class="collection-5"><a href="{{ $data->value }}" class="link-bottom" target="_blank">{{ $data->name }}</a></p>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-7">
                            <div class="bottom_title">Newsletter</div>
                            <p class="font-11">Follow us online to stay up to date<br/>with our news & events:</p>
                            <div class="sec2 form-newsletter">
                                {!! Form::open(['url' => '/subscribe']) !!}
                                    <input type="text" name="email" placeholder="Email" class="email"><input type="submit" name="submitSubscribeEmail" value="SUBSCRIBE" class="btn-subscribe">
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Follow us -->
            <div class="col-md-3 border bottom-height">
                <div class="bottom_body socmed">
                    <div class="bottom_title">Follow Us</div>
                    <p class="font-11">Follow us online to stay up to date<br/>with our news & events:</p>
                    <div class="sec2">
                        @if(Request::segment(1) == 'en')
                        <a href="{{ $fb->value }}" target="_blank" title="{{ $fb->title }}"><img src="{{ url('/') }}/web/assets/images/logo-fb.png" width="35"></a>
                        <a href="{{ $ig->value }}" target="_blank" title="{{ $ig->title }}"><img src="{{ url('/') }}/web/assets/images/instagram.png" width="35"></a>
                        <a href="{{ $tw->value }}" target="_blank" title="{{ $tw->title }}"><img src="{{ url('/') }}/web/assets/images/twitter.png" width="35"></a>
                        <a href="{{ $ta->value }}" target="_blank" title="{{ $ta->title }}"><img src="{{ url('/') }}/web/assets/images/tripadvisor.png" width="35"></a>
                        <a href="{{ $gp->value }}" target="_blank" title="{{ $gp->title }}"><img src="{{ url('/') }}/web/assets/images/gplus.png" width="35"></a>
                        @elseif(Request::segment(1) == 'id')
                        <a href="{{ $fb_id->value }}" target="_blank" title="{{ $fb_id->title }}"><img src="{{ url('/') }}/web/assets/images/logo-fb.png" width="35"></a>
                        <a href="{{ $ig_id->value }}" target="_blank" title="{{ $ig_id->title }}"><img src="{{ url('/') }}/web/assets/images/instagram.png" width="35"></a>
                        <a href="{{ $tw_id->value }}" target="_blank" title="{{ $tw_id->title }}"><img src="{{ url('/') }}/web/assets/images/twitter.png" width="35"></a>
                        <a href="{{ $ta_id->value }}" target="_blank" title="{{ $ta_id->title }}"><img src="{{ url('/') }}/web/assets/images/tripadvisor.png" width="35"></a>
                        <a href="{{ $gp_id->value }}" target="_blank" title="{{ $gp_id->title }}"><img src="{{ url('/') }}/web/assets/images/gplus.png" width="35"></a>
                        @endif
                    </div>
                </div>
            </div>

            <!-- Location -->
            <div class="col-md-3 border location bottom-height">
                <div class="bottom_body font-11">
                    <div class="bottom_title">Location<span class="map right"><a href="{{ $contact->map }}" target="_blank">MAP</a></span></div>
                    {!! html_entity_decode($contact->address) !!}
                    <p>t. {{ $contact->phone }}</p>
                    <p>f. {{ $contact->fax }}</p>
                    <p>{{ $contact->email }}</p>
                </div>
            </div>

        </div>
        <!-- End For Desktop and Tablet -->
    </div>
</div>
<!-- For Mobile -->
<div id="bottom" class="visible-md visible-sm visible-xs">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">COLLECTION</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                    @if(Request::segment(1) == 'en')
                        @foreach($collection as $item => $val)
                            @if($count_en <= 3)
                            <p><a href="{{ $val->value }}" class="link-bottom" target="_blank">{{ $val->name }}</a></p>
                            @elseif($count_en == 4)
                            <p class="collection-4"><a href="{{ $val->value }}" class="link-bottom" target="_blank">{{ $val->name }}</a></p>
                            @elseif($count_en == 5)
                            <p class="collection-5"><a href="{{ $val->value }}" class="link-bottom" target="_blank">{{ $val->name }}</a></p>
                            @endif
                        @endforeach
                    @elseif(Request::segment(1) == 'id')
                        @foreach($collection_id as $index => $data)
                            @if($count_id <= 3)
                                <p><a href="{{ $data->value }}" class="link-bottom" target="_blank">{{ $data->name }}</a></p>
                            @elseif($count_id == 4)
                                <p class="collection-4"><a href="{{ $data->value }}" class="link-bottom" target="_blank">{{ $data->name }}</a></p>
                            @elseif($count_id == 5)
                                <p class="collection-5"><a href="{{ $data->value }}" class="link-bottom" target="_blank">{{ $data->name }}</a></p>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">NEWSLETTER</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="font-11">Follow us online to stay up to date<br/>with our news & events:</p>
                    <div class="sec2 form-newsletter">
                        {!! Form::open(['url' => '/subscribe']) !!}
                            <input type="text" name="email" placeholder="Email" class="email"><input type="submit" name="submitSubscribeEmail" value="SUBSCRIBE" class="btn-subscribe">
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">FOLLOW US</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <p class="font-11">Follow us online to stay up to date<br/>with our news & events:</p>
                    <div class="sec2">
                        @if(Request::segment(1) == 'en')
                            <a href="{{ $fb->value }}" target="_blank" title="{{ $fb->title }}"><img src="{{ url('/') }}/web/assets/images/logo-fb.png" width="35"></a>
                            <a href="{{ $ig->value }}" target="_blank" title="{{ $ig->title }}"><img src="{{ url('/') }}/web/assets/images/instagram.png" width="35"></a>
                            <a href="{{ $tw->value }}" target="_blank" title="{{ $tw->title }}"><img src="{{ url('/') }}/web/assets/images/twitter.png" width="35"></a>
                            <a href="{{ $ta->value }}" target="_blank" title="{{ $ta->title }}"><img src="{{ url('/') }}/web/assets/images/tripadvisor.png" width="35"></a>
                            <a href="{{ $gp->value }}" target="_blank" title="{{ $gp->title }}"><img src="{{ url('/') }}/web/assets/images/gplus.png" width="35"></a>
                        @elseif(Request::segment(1) == 'id')
                            <a href="{{ $fb_id->value }}" target="_blank" title="{{ $fb_id->title }}"><img src="{{ url('/') }}/web/assets/images/logo-fb.png" width="35"></a>
                            <a href="{{ $ig_id->value }}" target="_blank" title="{{ $ig_id->title }}"><img src="{{ url('/') }}/web/assets/images/instagram.png" width="35"></a>
                            <a href="{{ $tw_id->value }}" target="_blank" title="{{ $tw_id->title }}"><img src="{{ url('/') }}/web/assets/images/twitter.png" width="35"></a>
                            <a href="{{ $ta_id->value }}" target="_blank" title="{{ $ta_id->title }}"><img src="{{ url('/') }}/web/assets/images/tripadvisor.png" width="35"></a>
                            <a href="{{ $gp_id->value }}" target="_blank" title="{{ $gp_id->title }}"><img src="{{ url('/') }}/web/assets/images/gplus.png" width="35"></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">LOCATION</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    {!! html_entity_decode($contact->address) !!}
                    <p>t. {{ $contact->phone }}</p>
                    <p>f. {{ $contact->fax }}</p>
                    <p>{{ $contact->email }}</p>
                    <span class="map right"><a href="{{ $contact->map }}" target="_blank">MAP</a></span>
                </div>
            </div>
        </div>

        <div class="over-link">
            <div class="">
                <div class="border-link text-center paddbottop-10">
                    @if(Request::segment(1) == 'en')
                        <a href="{{ url('/en/privacy-policy') }}">PRIVACY POLICY</a>
                    @elseif(Request::segment(1) == 'id')
                        <a href="{{ url('/id/privacy-policy') }}">PRIVACY POLICY</a>
                    @endif
                </div>
                <div class="border-link text-center paddbottop-10">
                    @if(Request::segment(1) == 'en')
                        <a href="{{ url('/en/terms-conditions') }}">TERMS & CONDITIONS</a>
                    @elseif(Request::segment(1) == 'id')
                        <a href="{{ url('/id/terms-conditions') }}">TERMS & CONDITIONS</a>
                    @endif
                </div>
                <div class="copy-right text-center"><p>@2017 Juno Hotel All rights reserved</p></div>
            </div>
        </div>
    </div>
</div>
<!-- End For Mobile -->