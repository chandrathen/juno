<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role==1) {
            Session::put('lang', 'eng');
            return redirect('admin/homepage');
        }
        else {
            Session::put('lang', 'eng');
            return redirect('admin/homepage');
        }
    }

    public function changelang(Request $req)
    {
        Session::put('lang', $req->lan);

        return 1;
    }
}
