@extends('layouts.web.frame')

@section('content')
    @if($detail->menu_title == 'happening')
        <div id="happening_container" class="container">
            @foreach($detail_hap as $item => $data)
            <div class="content-happening">
                <div class="row">
                    <div class="col-md-6">
                        <div style="background-image: url({{ url('/uploads/'. $data->image)}}); background-size: cover; background-position: center; height: 32vw;"></div>
                    </div>
                    <div class="col-md-6">
                        {!! html_entity_decode($data->description) !!}
                    </div>
                </div>
            </div>
            <hr>
            @endforeach
        </div>

    @elseif($detail->menu_title == 'event')
        <div id="happening_container" class="container">
            @foreach($detail_hap as $item => $data)
                <div class="content-happening">
                    <div class="row">
                        <div class="col-md-6">
                            <div style="background-image: url({{ url('/uploads/'. $data->image)}}); background-size: cover; background-position: center; height: 32vw;"></div>
                        </div>
                        <div class="col-md-6">
                            {!! html_entity_decode($data->description) !!}
                        </div>
                    </div>
                </div>
                <hr>
            @endforeach
        </div>

    @else

    <div id="detail_content" class="hidden-sm hidden-xs">
        <div id="detail_wrapper" class="container-fluid">
            <div id="detail" class="row">
                <div class="col-md-6">
                    <div class="left-content">
                        <div class="detail-title">
                            <h1>{{ $detail->title }}</h1>
                        </div>
                        <div class="desc_detail">
                            {!! html_entity_decode($detail->description) !!}
                        </div>
                        <div class="nav-detail">
                            @if(Request::segment(1) == 'en')

                                @if(Request::segment(4) <= $min)
                                    <div class="btn-prev-content" style="display: none"><a href="{{ url('/en/'.$menu.'/detail/'.$previous['id']) }}">Previous<span class="icon-chevron-left"></span></a></div>
                                @else
                                    <div class="btn-prev-content"><a href="{{ url('/en/'.$menu.'/detail/'.$previous['id']) }}">Previous<span class="icon-chevron-left"></span></a></div>
                                @endif

                                @if(Request::segment(4) >= $max)
                                    <div class="btn-next-content" style="display: none"><a href="{{ url('/en/'.$menu.'/detail/'.$next['id']) }}">Next<span class="icon-chevron-right"></span></a></div>
                                @else
                                    <div class="btn-next-content"><a href="{{ url('/en/'.$menu.'/detail/'.$next['id']) }}">Next<span class="icon-chevron-right"></span></a></div>
                                @endif

                            @elseif(Request::segment(1) == 'id')
                                
                                @if(Request::segment(4) <= $min)
                                    <div class="btn-prev-content" style="display: none"><a href="{{ url('/id/'.$menu.'/detail/'.$previous['id']) }}">Previous<span class="icon-chevron-left"></span></a></div>
                                @else
                                    <div class="btn-prev-content"><a href="{{ url('/id/'.$menu.'/detail/'.$previous['id']) }}">Previous<span class="icon-chevron-left"></span></a></div>
                                @endif

                                @if(Request::segment(4) >= $max)
                                    <div class="btn-next-content" style="display: none"><a href="{{ url('/id/'.$menu.'/detail/'.$next['id']) }}">Next<span class="icon-chevron-right"></span></a></div>
                                @else
                                    <div class="btn-next-content"><a href="{{ url('/id/'.$menu.'/detail/'.$next['id']) }}">Next<span class="icon-chevron-right"></span></a></div>
                                @endif
                            @endif

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <ul id="bxslider">
                        @foreach($image as $index => $val)
                            <li><img src="{{ url('/uploads/'. $val->image) }}"/></li>
                        @endforeach
                    </ul>

                    <ul id="bxslider-pager">
                        @php $i=0; @endphp
                        @foreach($image as $item => $data)
                            <li data-slideIndex="{{ $i }}"><a href=""><img src="{{ url('/uploads/' .$data->image) }}"/></a></li>
                        @php $i++; @endphp
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div id="detail_content" class="visible-sm visible-xs">
        <div id="detail_wrapper" class="container-fluid">
            <div id="detail" class="row">
                <div class="col-md-6">
                    <ul id="bxslider-2">
                        @foreach($image as $index => $val)
                            <li><img src="{{ url('/uploads/'. $val->image) }}" alt=""></li>
                        @endforeach
                    </ul>

                    <ul id="bxslider-pager-2">
                        @php $i=0; @endphp
                        @foreach($image as $item => $data)
                            <li data-slideIndex="{{ $i }}"><a href=""><img src="{{ url('/uploads/' .$data->image) }}"></a></li>
                        @php $i++; @endphp
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="left-content">
                        <div class="detail-title">
                            <h1>{{ $detail->title }}</h1>
                        </div>
                        <div class="desc_detail">
                            {!! html_entity_decode($detail->description) !!}
                        </div>
                        <div class="nav-detail">
                            @if(Request::segment(1) == 'en')

                                @if(Request::segment(4) <= $min)
                                    <div class="btn-prev-content" style="display: none"><a href="{{ url('/en/'.$menu.'/detail/'.$previous['id']) }}">Previous<span class="icon-chevron-left"></span></a></div>
                                @else
                                    <div class="btn-prev-content"><a href="{{ url('/en/'.$menu.'/detail/'.$previous['id']) }}">Previous<span class="icon-chevron-left"></span></a></div>
                                @endif

                                @if(Request::segment(4) >= $max)
                                    <div class="btn-next-content" style="display: none"><a href="{{ url('/en/'.$menu.'/detail/'.$next['id']) }}">Next<span class="icon-chevron-right"></span></a></div>
                                @else
                                    <div class="btn-next-content"><a href="{{ url('/en/'.$menu.'/detail/'.$next['id']) }}">Next<span class="icon-chevron-right"></span></a></div>
                                @endif

                            @elseif(Request::segment(1) == 'id')
                                
                                @if(Request::segment(4) <= $min)
                                    <div class="btn-prev-content" style="display: none"><a href="{{ url('/id/'.$menu.'/detail/'.$previous['id']) }}">Previous<span class="icon-chevron-left"></span></a></div>
                                @else
                                    <div class="btn-prev-content"><a href="{{ url('/id/'.$menu.'/detail/'.$previous['id']) }}">Previous<span class="icon-chevron-left"></span></a></div>
                                @endif

                                @if(Request::segment(4) >= $max)
                                    <div class="btn-next-content" style="display: none"><a href="{{ url('/id/'.$menu.'/detail/'.$next['id']) }}">Next<span class="icon-chevron-right"></span></a></div>
                                @else
                                    <div class="btn-next-content"><a href="{{ url('/id/'.$menu.'/detail/'.$next['id']) }}">Next<span class="icon-chevron-right"></span></a></div>
                                @endif
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

@endsection