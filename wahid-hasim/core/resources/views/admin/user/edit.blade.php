@extends('layouts.app.frame')
@section('title', 'Edit User')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('user.edit', $user))
@section('button', '<a href="'.url('/admin/user').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::model($user, [
            'method' => 'PATCH',
            'url' => ['/admin/user', $user->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}
        				
        @include ('admin.user.form', ['submitButtonText' => 'Update'])
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush