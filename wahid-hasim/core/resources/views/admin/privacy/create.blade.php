@extends('layouts.app.frame')
@section('title', 'Create New Privacy')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('privacy.new'))
@section('button', '<a href="'.url('/admin/privacy').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/privacy', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.privacy.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
tinymce.init({
    selector:'textarea',
    plugins: "code",
    menubar: "file edit insert view format table tools",
    height : 100
});
</script>
@endpush