@extends('layouts.app.frame')
@section('title', 'Create New Whatson')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('whatson.new'))
@section('button', '<a href="'.url('/admin/whatson').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/whatson', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.whatson.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush