<div aria-required="true" class="form-group form-group-default required {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('name', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('value') ? 'has-error' : ''}}">
    {!! Form::label('value', 'Value') !!}
    {!! Form::textarea('value', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('value', '<label class="error">:message</label>') !!}

@if(isset($lang))
    <input type="hidden" id="language" name="language" value="{{ $lang }}">
@endif

<div class="pull-left">
    <div class="checkbox check-success">
        <input id="checkbox-agree" type="checkbox" required> <label for="checkbox-agree">Saya sudah mengecek data sebelum menyimpan</label>
    </div>
</div>
<br/>

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<button class="btn btn-default" type="reset"><i class="pg-close"></i> Clear</button>
