<script data-pace-options='{ "ajax": true }' src="{{ url('plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/bootstrapv3/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/jquery-bez/jquery.bez.min.js') }}"></script>
<script src="{{ url('plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/jquery-actual/jquery.actual.min.js') }}"></script>
<script src="{{ url('plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
<script src="{{ url('plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/classie/classie.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/nvd3/lib/d3.v3.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/nvd3/nv.d3.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/nvd3/src/utils.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/nvd3/src/tooltip.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/nvd3/src/interactiveLayer.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/nvd3/src/models/axis.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/nvd3/src/models/line.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/nvd3/src/models/lineWithFocusChart.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/mapplic/js/hammer.js') }}"></script>
<script src="{{ url('plugins/mapplic/js/jquery.mousewheel.js') }}"></script>
<script src="{{ url('plugins/mapplic/js/mapplic.js') }}"></script>
<script src="{{ url('plugins/rickshaw/rickshaw.min.js') }}"></script>
<script src="{{ url('plugins/jquery-metrojs/MetroJs.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/jquery-sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/skycons/skycons.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/jquery-datatable/extensions/FixedHeader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ url('plugins/jquery-datatable/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('plugins/jquery-datatable/extensions/Buttons/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ url('plugins/jquery-datatable/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ url('plugins/jquery-datatable/extensions/Buttons/js/jszip.js') }}"></script>
<script src="{{ url('plugins/jquery-datatable/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ url('plugins/jquery-datatable/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ url('plugins/jquery-datatable/extensions/Buttons/js/buttons.colVis.min.js') }}"></script>
<script src="{{ url('plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
<script src="{{ url('plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{url('plugins/jquery-validation/js/jquery.validate.min.js')}}"></script>
<script src="{{ url('js/pages.min.js') }}"></script>
<script src="{{ url('js/moment.js') }}"></script>
<script src="{{ url('js/scripts.js') }}"></script>
<script src="{{ url('js/dropzone.js') }}"></script>
<script src="{{ url('js/dropzone-amd-module.js') }}"></script>
<!--FancyBox-->
<!-- Add mousewheel plugin (this is optional) -->
<!-- Add fancyBox -->
<link rel="stylesheet" href="{{asset('plugins/fancybox/source/jquery.fancybox.css?v=2.1.6')}}" type="text/css" media="screen" />
<script type="text/javascript" src="{{asset('plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.6')}}"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="{{asset('plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')}}" type="text/css" media="screen" />
<script type="text/javascript" src="{{asset('plugins/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')}}"></script>
<script type="text/javascript" src="{{asset('plugins/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6')}}"></script>

<link rel="stylesheet" href="{{asset('plugins/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7')}}" type="text/css" media="screen" />
<script type="text/javascript" src="{{asset('plugins/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('body').addClass('sidebar-visible menu-pin');
});
</script>