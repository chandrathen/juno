    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('/') }}/web/assets/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('/') }}/web/assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('/') }}/web/assets/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/') }}/web/assets/images/favicon/favicon-16x16.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
        
    <title>Juno Hotel CMS - @yield('title')</title>
    
    @section('header')
        @include('layouts.app.header')
    @show
    
</head>

<body class="fixed-header dashboard sidebar-visible menu-pin">
    <nav class="page-sidebar" data-pages="sidebar">        

        <div class="sidebar-header">
            <img alt="logo" class="brand" data-src="{{ url('assets/images/logo/logo-white.png') }}" data-src-retina="{{ url('assets/images/logo/logo-white.png') }}" src="{{ url('assets/images/logo/logo-white.png') }}" width="78">
            <div class="sidebar-header-controls"> <button class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar" type="button"><i class="fa fs-12"></i></button>
            </div>
        </div>

        <div class="sidebar-menu">
            <ul class="menu-items">
                @include('layouts.app.menus.admin')
            </ul>

            <div class="clearfix"></div>
        </div>
    </nav>
    <div class="page-container">
		<div class="header">
			<div class="container-fluid relative">
				<div class="pull-left full-height visible-sm visible-xs">
					<div class="header-inner">
						<a class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar" href="#"><span class="icon-set menu-hambuger"></span></a>
					</div>
				</div>


				<div class="pull-center hidden-md hidden-lg">
					<div class="header-inner">
						<div class="brand inline"><img alt="logo" data-src="{{ url('assets/images/logo/1.png') }}" data-src-retina="{{ url('assets/images/logo/1.png') }}" height="22" src="{{ url('assets/images/logo/1.png') }}" width="78">
						</div>
					</div>
				</div>


				<div class="pull-right full-height visible-sm visible-xs">
					<div class="header-inner">
						<a class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview" href="#"><span class="icon-set menu-hambuger-plus"></span></a>
					</div>
				</div>
			</div>




			<div class=" pull-right">
				<div class="visible-lg visible-md m-t-10">
					<div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
						<span class="semi-bold">{{ Auth::User()->name }}</span>
					</div>


					<div class="dropdown pull-right">
						<button aria-expanded="false" aria-haspopup="true" class="profile-dropdown-toggle" data-toggle="dropdown" type="button"><span class="thumbnail-wrapper d32 circular inline m-t-5"><img alt="" data-src="{{ url('img/profiles/avatar.jpg') }}" data-src-retina="{{ url('img/profiles/avatar_small2x.jpg') }}" height="32" src="{{ url('img/profiles/avatar.jpg') }}" width="32"></span></button>

						<ul class="dropdown-menu profile-dropdown" role="menu">
							<li>
								<a href="{{url('admin/profile')}}"><i class="pg-settings_small"></i> Settings</a>
							</li>
							<li class="bg-master-lighter">
								<a class="clearfix" href="{{url('logout')}}"><span class="pull-left">Logout</span> <span class="pull-right"><i class="pg-power"></i></span></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>


		<div class="page-content-wrapper bg-white">
			<div class="content">
				<div class="jumbotron" data-pages="parallax">
                    <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                        <div class="inner" style="transform: translateY(0px); opacity: 1;">
                            @yield('breadcrumbs', Breadcrumbs::render('home'))
                        </div>
                    </div>
                </div>

				<div class="container-fluid container-fixed-lg">
                    <div class="row">
                        <div class="col-sm-12 no-padding">
                            <div class="panel panel-transparent">                               
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        @yield("title", "Title")
                                    </div>
                                    <div class="pull-right">
                                        <div class="col-xs-12 no-padding">                                        
                                            <div class="export-options-container"></div>
                                        </div>
                                    </div>
                                    @if(Request::is('admin/user') || Request::is('admin/dashboard') || Request::is('admin/appliance'))
                                        <div class="clearfix"></div>
                                    @endif
                                    @yield('button')
                                </div>
                                <div class="panel-body ">                                   
                                    @yield("content")
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			
			<!-- MODAL STICK UP  -->
            <div class="modal fade stick-up" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5>Hapus <span class="semi-bold">Data?</span></h5>
                        </div>
                        <div class="modal-body text-center">
                            <button class="btn btn-danger btn-sm" onclick="hapus()">Ya, hapus</button>
                        </div>
                    </div>
                </div>
            </div>


            @include('layouts.app.footer')
		</div>
	</div>
   
    @section('scripts')
        @include('layouts.app.script')
    @stack('script')        
</body>
</html>
