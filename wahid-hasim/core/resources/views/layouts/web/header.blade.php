<!--Styles-->
    <link href="{{ url('/') }}/web/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/web/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/') }}/web/assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/') }}/web/assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ url('/') }}/web/assets/css/superslides.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/') }}/web/assets/css/jquery.bxslider.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/') }}/web/assets/css/flexslider.css" rel="stylesheet" type="text/css">
    <link href="{{ url('/') }}/web/assets/css/owl.carousel.css" rel="stylesheet" type="text/css">

    <!--Fonts-->
    <link href="{{ url('/') }}/web/assets/fonts/gotham/style.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/web/assets/fonts/myriadpro/style.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/web/assets/fonts/icomoon/style.css" rel="stylesheet" type="text/css"/>

    <!--Mechanic Styles-->
    <link rel="stylesheet" href="{{ url('/') }}/web/assets/css/default/style.css">
    <link rel="stylesheet" href="{{ url('/') }}/web/assets/css/default/content.css">
    <link rel="stylesheet" href="{{ url('/') }}/web/assets/css/default/default.css">
    <link rel="stylesheet" href="{{ url('/') }}/web/assets/css/responsive/responsive.css">


<!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/web/vendors/rs-plugin/css/settings-ie8.css" media="screen">
<script src="{{ url('/') }}/web/js/html5shiv.min.js"></script>
<script src="{{ url('/') }}/web/js/respond.min.js"></script>
<![endif]-->