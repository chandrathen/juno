<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="Juno Hotel" name="description" />
    <meta content="Juno Hotel" name="author" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/') }}/web/assets/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('/') }}/web/assets/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('/') }}/web/assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('/') }}/web/assets/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/') }}/web/assets/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Juno Hotel</title>

    @include('layouts.web.header')
    <style>
        .wide-container {
            height: 38.5vw;
        }
        #content_container.clear-min-height {
            min-height: inherit;
        }
    </style>


  </head>

@if(Request::segment(2) == '')
    <body data-spy="scroll" data-target="#main_nav" class="default">
@else
    <body id="container-content">
@endif

  @if(Request::segment(2) == '')
  <header id="main-header">
      <div class="navbar navbar-default navbar-fat navbar-clean">
          <div class="container-fluid">
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                      <span class="icon-bar b1"></span>
                      <span class="icon-bar b2"></span>
                      <span class="icon-bar b3"></span>
                  </button>
                    @if(Request::segment(1) == 'en')
                        <a class="navbar-brand" href="{{ url('/en') }}">
                    @elseif(Request::segment(1) == 'id')
                        <a class="navbar-brand" href="{{ url('/id') }}">
                    @endif
                      <img src="{{ url('/') }}/web/assets/images/logo-white.png">
                  </a>
              </div>
              <div class="navbar-collapse collapse">
                  <ul id="main-nav" class="nav navbar-nav navbar-right nav-white">
                      <li class="menu">
                          @if(Request::segment(1) == 'en')
                              <a href="{{ url('/en/room') }}">Room</a>
                          @elseif(Request::segment(1) == 'id')
                              <a href="{{ url('/id/room') }}">Room</a>
                          @endif
                      </li>

                      <li class="menu">
                          @if(Request::segment(1) == 'en')
                              <a href="{{ url('/en/meeting') }}">Meeting</a>
                          @elseif(Request::segment(1) == 'id')
                              <a href="{{ url('/id/meeting') }}">Meeting</a>
                          @endif
                      </li>

                      <li class="menu">
                          @if(Request::segment(1) == 'en')
                            <a href="{{ url('/en/facilities') }}">Facilities</a>
                          @elseif(Request::segment(1) == 'id')
                            <a href="{{ url('/id/facilities') }}">Facilities</a>
                          @endif
                      </li>

                      <li class="menu">
                          @if(Request::segment(1) == 'en')
                              <a href="{{ url('/en/gallery') }}">Gallery</a>
                          @elseif(Request::segment(1) == 'id')
                              <a href="{{ url('/id/gallery') }}">Gallery</a>
                          @endif
                      </li>

                      <li class="menu">
                          @if(Request::segment(1) == 'en')
                              <a href="{{ url('/en/neighbourhood') }}">Neighbourhood</a>
                          @elseif(Request::segment(1) == 'id')
                              <a href="{{ url('/id/neighbourhood') }}">Neighbourhood</a>
                          @endif
                      </li>

                      <li class="menu dropdown">
                          @if(Request::segment(1) == 'en')
                            <a href="{{ url('/en/whatson') }}" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">What's on</a>
                          @elseif(Request::segment(1) == 'id')
                            <a href="{{ url('/id/whatson') }}" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">What's on</a>
                          @endif
                          <ul class="dropdown-menu">
                              @if(Request::segment(1) == 'en')
                                  @foreach($w as $asc => $val)
                                      @if(Request::segment(1) == 'en')
                                        @if($val->menu_title == 'event')
                                            <li><a href="{{ url('/en/event/detail/'. $val->id) }}">Event</a></li>
                                        @elseif($val->menu_title == 'happening')
                                            <li><a href="{{ url('/en/happening/detail/'. $val->id) }}">Happening</a></li>
                                        @endif
                                      @elseif(Request::segment(1) == 'id')
                                          @if($val->menu_title == 'event')
                                              <li><a href="{{ url('/id/event/detail/'. $val->id) }}">Event</a></li>
                                          @elseif($val->menu_title == 'happening')
                                              <li><a href="{{ url('/id/happening/detail/'. $val->id) }}">Happening</a></li>
                                          @endif
                                      @endif
                                  @endforeach
                              @elseif(Request::segment(1) == 'id')
                                  @foreach($w_id as $index => $item)
                                      @if(Request::segment(1) == 'en')
                                          @if($item->menu_title == 'event')
                                              <li><a href="{{ url('/en/event/detail/'. $item->id) }}">Event</a></li>
                                          @elseif($item->menu_title == 'happening')
                                              <li><a href="{{ url('/en/happening/detail/'. $item->id) }}">Happening</a></li>
                                          @endif
                                      @elseif(Request::segment(1) == 'id')
                                          @if($item->menu_title == 'event')
                                              <li><a href="{{ url('/id/event/detail/'. $item->id) }}">Event</a></li>
                                          @elseif($item->menu_title == 'happening')
                                              <li><a href="{{ url('/id/happening/detail/'. $item->id) }}">Happening</a></li>
                                          @endif
                                      @endif
                                  @endforeach
                              @endif
                          </ul>
                      </li>
                      <li class="lang-switch right">
                          @if(Request::segment(1) == 'en')
                            <a class="eng btn-lang active" href="{{ url('/en') }}">Eng</a>
                          @else
                            <a class="eng btn-lang" href="{{ url('/en') }}">Eng</a>
                          @endif
                          <span class="liner"></span>
                          @if(Request::segment(1) == 'id')
                            <a class="ind btn-lang active" href="{{ url('/id') }}">Ind</a>
                          @else
                            <a class="ind btn-lang" href="{{ url('/id') }}">Ind</a>
                          @endif
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </header>
  @else
      <header id="main-header">
          <div class="navbar navbar-default navbar-fat navbar-clean">
              <div class="container-fluid">
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                          <span class="icon-bar b1"></span>
                          <span class="icon-bar b2"></span>
                          <span class="icon-bar b3"></span>
                      </button>
                      @if(Request::segment(1) == 'en')
                          <a class="navbar-brand" href="{{ url('/en') }}">
                      @elseif(Request::segment(1) == 'id')
                          <a class="navbar-brand" href="{{ url('/id') }}">
                      @endif
                          <img src="{{ url('/') }}/web/assets/images/logo.png">
                      </a>
                  </div>
                  <div class="navbar-collapse collapse">
                      <ul id="main-nav" class="nav navbar-nav navbar-right nav-black">
                          <li class="menu {{ Request::segment(2) == 'room' ? 'active' : '' }}">
                              @if(Request::segment(1) == 'en')
                                <a href="{{ url('/en/room') }}">Room</a>
                              @elseif(Request::segment(1) == 'id')
                                <a href="{{ url('/id/room') }}">Room</a>
                              @endif
                          </li>

                          <li class="menu {{ Request::segment(2) == 'meeting' ? 'active' : '' }}">
                              @if(Request::segment(1) == 'en')
                                <a href="{{ url('/en/meeting') }}">Meeting</a>
                              @elseif(Request::segment(1) == 'id')
                                <a href="{{ url('/id/meeting') }}">Meeting</a>
                              @endif
                          </li>

                          <li class="menu {{ Request::segment(2) == 'facilities' ? 'active' : '' }}">
                              @if(Request::segment(1) == 'en')
                                  <a href="{{ url('/en/facilities') }}">Facilities</a>
                              @elseif(Request::segment(1) == 'id')
                                  <a href="{{ url('/id/facilities') }}">Facilities</a>
                              @endif
                          </li>

                          <li class="menu {{ Request::segment(2) == 'gallery' ? 'active' : '' }}">
                              @if(Request::segment(1) == 'en')
                                <a href="{{ url('/en/gallery') }}">Gallery</a>
                              @elseif(Request::segment(1) == 'id')
                                <a href="{{ url('/id/gallery') }}">Gallery</a>
                              @endif
                          </li>

                          <li class="menu {{ Request::segment(2) == 'neighbourhood' ? 'active' : '' }}">
                              @if(Request::segment(1) == 'en')
                                  <a href="{{ url('/en/neighbourhood') }}">Neighbourhood</a>
                              @elseif(Request::segment(1) == 'id')
                                  <a href="{{ url('/id/neighbourhood') }}">Neighbourhood</a>
                              @endif
                          </li>

                          <li class="menu {{ Request::segment(2) == 'whatson' ? 'active' : '' }} dropdown">
                              @if(Request::segment(1) == 'en')
                                <a href="{{ url('/en/whatson') }}" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">What's on</a>
                              @elseif(Request::segment(1) == 'id')
                                <a href="{{ url('/id/whatson') }}" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">What's on</a>
                              @endif
                              <ul class="dropdown-menu">
                                  @if(Request::segment(1) == 'en')
                                      @foreach($w as $asc => $val)
                                          @if(Request::segment(1) == 'en')
                                              @if($val->menu_title == 'event')
                                                  <li><a href="{{ url('/en/event/detail/'. $val->id) }}">Event</a></li>
                                              @elseif($val->menu_title == 'happening')
                                                  <li><a href="{{ url('/en/happening/detail/'. $val->id) }}">Happening</a></li>
                                              @endif
                                          @elseif(Request::segment(1) == 'id')
                                              @if($val->menu_title == 'event')
                                                  <li><a href="{{ url('/id/event/detail/'. $val->id) }}">Event</a></li>
                                              @elseif($val->menu_title == 'happening')
                                                  <li><a href="{{ url('/id/happening/detail/'. $val->id) }}">Happening</a></li>
                                              @endif
                                          @endif
                                      @endforeach
                                  @elseif(Request::segment(1) == 'id')
                                      @foreach($w_id as $index => $item)
                                          @if(Request::segment(1) == 'en')
                                              @if($item->menu_title == 'event')
                                                  <li><a href="{{ url('/en/event/detail/'. $item->id) }}">Event</a></li>
                                              @elseif($item->menu_title == 'happening')
                                                  <li><a href="{{ url('/en/happening/detail/'. $item->id) }}">Happening</a></li>
                                              @endif
                                          @elseif(Request::segment(1) == 'id')
                                              @if($item->menu_title == 'event')
                                                  <li><a href="{{ url('/id/event/detail/'. $item->id) }}">Event</a></li>
                                              @elseif($item->menu_title == 'happening')
                                                  <li><a href="{{ url('/id/happening/detail/'. $item->id) }}">Happening</a></li>
                                              @endif
                                          @endif
                                      @endforeach
                                  @endif
                              </ul>
                          </li>
                          <li class="lang-switch right">
                              @if(Request::segment(1) == 'en')
                                  <a class="eng btn-lang active" href="{{ url('/en') }}">Eng</a>
                              @else
                                  <a class="eng btn-lang" href="{{ url('/en') }}">Eng</a>
                              @endif
                              <span class="liner"></span>
                              @if(Request::segment(1) == 'id')
                                  <a class="ind btn-lang active" href="{{ url('/id') }}">Ind</a>
                              @else
                                  <a class="ind btn-lang" href="{{ url('/id') }}">Ind</a>
                              @endif
                          </li>
                      </ul>
                  </div>
                  <div class="drop-show right">
                      <div class="collapse collapse-book" id="collapse">
                          <div class="">
                              <div id="booking">
                                  <div class="booking_area">

                                      <form method="post" target="_blank" action="http://juno-hotel.com/book" id="booking_form">
                                          <div class="form-group title">Book Now!</div>

                                          <div class="form-group" style="position:relative;">
                                              <div class="row">
                                                  <div class="col-sm-6 paddright-5">
                                                      <div class="input-group date marginbott-5">
                                                          <div id="datepicker-from">
                                                             <font id="-from">From</font>
                                                             <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="col-sm-6 paddleft-5">
                                                      <div class="input-group date marginbott-5">
                                                          <div id="datepicker-to" class="pull-right">
                                                             <font id="-to">To</font>
                                                             <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <div class="row">
                                                  <div class="col-sm-6 paddright-5">
                                                      <input id="guest" class="open_dropdown marginbott-5" type="text" name="guest" placeholder="Guest" readonly="readonly" sub="guest">
                                                      <div id="guest_list" class="dropdown" sub="guest" style="display: none;">
                                                          <p for="guest">1</p>
                                                          <p for="guest">2</p>
                                                          <p for="guest">3</p>
                                                      </div>
                                                      <span id="guest_icon" class="icon-user-o open_dropdown" style="font-size:16px;" sub="guest"></span>
                                                  </div>
                                                  <div class="col-sm-6 paddleft-5">
                                                      <input id="rooms" class="open_dropdown marginbott-5" type="text" name="rooms" placeholder="Rooms" readonly="readonly" sub="rooms">
                                                      <div id="rooms_list" class="dropdown" sub="rooms" style="display: none;">
                                                          <p for="rooms">1</p>
                                                          <p for="rooms">2</p>
                                                          <p for="rooms">3</p>
                                                      </div>
                                                      <span id="rooms_icon" class="icon-chevron-with-circle-right open_dropdown" style="font-size:18px;" sub="rooms"></span>
                                                  </div>
                                              </div>
                                          </div>

                                          <div class="check-avail">
                                              <a class="btn-avail" href="#">Check Availability</a>
                                          </div>
                                      </form>

                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="btn-book-header">
                      <a href="#collapse" data-toggle="collapse" aria-expanded="false" aria-controls="collapse">Book Now</a>
                  </div>
              </div>
          </div>
      </header>
      @endif

    @yield('content')

    @include('layouts/web/footer')
    @include('layouts.web.script')

    @yield('js-script')

  </body>
</html>
