<script src="{{ url('/') }}/web/assets/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/web/assets/js/jquery.animate-enhanced.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/web/assets/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="{{ url('/') }}/web/assets/js/jquery.bxslider.js" type="text/javascript"></script>
<script src="{{ url('/') }}/web/assets/js/jquery.superslides.js" type="text/javascript"></script>
<script src="{{ url('/') }}/web/assets/js/owl.carousel.js" type="text/javascript"></script>
<script src="{{ url('/') }}/web/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/web/assets/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="{{ url('/') }}/web/assets/js/jquery-ui.js" type="text/javascript"></script>
<script src="{{ url('/') }}/web/assets/js/jquery.flexslider.js" type="text/javascript"></script>
<script src="{{ url('/') }}/web/assets/js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="{{ url('/') }}/web/assets/js/jpreloader.js" type="text/javascript"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>   
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script> 
<script>
    jQuery(document).ready(function ($) {
        $('#slides').superslides({
            //play: 5000,
            pagination: true
        });
        $('#slides2').superslides({
            //play: 5000,
            inherit_width_from: '.wide-container',
            inherit_height_from: '.wide-container',
            pagination: true
        });
    });
    document.getElementById("loader").style.display = "none";
    document.getElementById("myLoader").style.display = "block";
</script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var $j = jQuery.noConflict();
        var realSlider= $j("ul#bxslider").bxSlider({
            speed:1000,
            pager:false,
            controls:true,
            nextText:'',
            prevText:'',
            infiniteLoop:false,
            hideControlOnEnd:true,
            onSlideBefore:function($slideElement, oldIndex, newIndex){
                changeRealThumb(realThumbSlider,newIndex);
            }
        });
        var realThumbSlider=$j("ul#bxslider-pager").bxSlider({
            minSlides: 3,
            maxSlides: 3,
            slideWidth: 356,
            slideMargin: 12,
            moveSlides: 1,
            pager:false,
            speed:1000,
            infiniteLoop:false,
            hideControlOnEnd:true,
            nextText:'<span></span>',
            prevText:'<span></span>',
            onSlideBefore:function($slideElement, oldIndex, newIndex){
                /*$j("#sliderThumbReal ul .active").removeClass("active");
                $slideElement.addClass("active"); */
            }
        });
        linkRealSliders(realSlider,realThumbSlider);
        if($j("#bxslider-pager li").length<3){
            $j("#bxslider-pager .bx-next").hide();
        }
        // sincronizza sliders realizzazioni
        function linkRealSliders(bigS,thumbS){
            $j("ul#bxslider-pager").on("click","a",function(event){
                event.preventDefault();
                var newIndex=$j(this).parent().attr("data-slideIndex");
                bigS.goToSlide(newIndex);
            });
        }
        //slider!=$thumbSlider. slider is the realslider
        function changeRealThumb(slider,newIndex){
            var $thumbS=$j("#bxslider-pager");
            $thumbS.find('.active').removeClass("active");
            $thumbS.find('li[data-slideIndex="'+newIndex+'"]').addClass("active");
            if(slider.getSlideCount()-newIndex>=20)slider.goToSlide(newIndex);
            else slider.goToSlide(slider.getSlideCount()-20);
        }
    });
    jQuery(document).ready(function ($) {
        var $j = jQuery.noConflict();  
        var realSlider= $j("#bxslider-2").bxSlider({
            speed:1000,
            pager:false,
            controls:true,
            nextText:'',
            prevText:'',
            infiniteLoop:false,
            hideControlOnEnd:true,
            onSlideBefore:function($slideElement, oldIndex, newIndex){
                changeRealThumb(realThumbSlider,newIndex);
            }
        });
        var realThumbSlider=$j("#bxslider-pager-2").bxSlider({
            minSlides: 3,
            maxSlides: 3,
            slideWidth: 356,
            slideMargin: 12,
            moveSlides: 1,
            pager:false,
            speed:1000,
            infiniteLoop:false,
            hideControlOnEnd:true,
            nextText:'<span></span>',
            prevText:'<span></span>',
            onSlideBefore:function($slideElement, oldIndex, newIndex){
                /*$j("#sliderThumbReal ul .active").removeClass("active");
                $slideElement.addClass("active"); */
            }
        });
        linkRealSliders(realSlider,realThumbSlider);
        if($j("#bxslider-pager-2 li").length<3){
            $j("#bxslider-pager-2 .bx-next").hide();
        }
        // sincronizza sliders realizzazioni
        function linkRealSliders(bigS,thumbS){
            $j("#bxslider-pager-2").on("click","a",function(event){
                event.preventDefault();
                var newIndex=$j(this).parent().attr("data-slideIndex");
                bigS.goToSlide(newIndex);
            });
        }
        //slider!=$thumbSlider. slider is the realslider
        function changeRealThumb(slider,newIndex){
            var $thumbS=$j("#bxslider-pager-2");
            $thumbS.find('.active').removeClass("active");
            $thumbS.find('li[data-slideIndex="'+newIndex+'"]').addClass("active");
            if(slider.getSlideCount()-newIndex>=20)slider.goToSlide(newIndex);
            else slider.goToSlide(slider.getSlideCount()-20);
        }
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var jQuerywindow = jQuery(window),
            flexslider;
        // tiny helper function to add breakpoints
        function getGridSize() {
            return (window.innerWidth < 600) ? 2 : (window.innerWidth < 900) ? 3 : 3;
        }
        $(window).load(function() {
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                asNavFor: '#slider',
                moveSlides: 1,
                itemWidth: 170,
                itemMargin: 5,
                minItems: getGridSize(),
                maxItems: getGridSize()
            });

            $('#slider').flexslider({
                animation: "slide",
                smoothHeight: true,
                controlNav: false,
                animationLoop: false,
                directionNav: false,
                slideshow: true,
                sync: "#carousel",
                start: function(slider) {
                    flexslider = slider;
                }
            });
            // check grid size on resize event
            jQuery(window).resize(function () {
                var gridSize = getGridSize();

                flexslider.vars.minItems = gridSize;
                flexslider.vars.maxItems = gridSize;
            });
        });
    });
</script>
<script>
    jQuery(document).ready(function ($) {
        
        $('#datepicker-from').daterangepicker({
            "singleDatePicker": true,
            "opens": "right",
            "dateLimit": {
                "days": 7
            },
            "autoUpdateInput" : false,
        }, function(start, end, label) {
          $("#-from").html(start.format('YYYY-MM-DD'));
        });
        
        $('#datepicker-to').daterangepicker({
            "singleDatePicker": true,
            "opens": "left",
            "dateLimit": {
                "days": 7
            },
            "autoUpdateInput" : false,
        }, function(start, end, label) {
          $("#-to").html(start.format('YYYY-MM-DD'));
        });
    
        /* Menu booking scroll
        ==================================*/
        //If class open_dropdown clicked, open dropdown where had sub[custom] attribute
        $('.open_dropdown').click(function(){
            var tmp = $(this).attr('sub');
            $('.dropdown').hide();
            $('.dropdown[sub="' + tmp + '"]').show();
        });

        //If icon clicked
        $('.icon').click(function(){
            var id      = $(this).attr('id');
            var temp    = id.split('_icon');
            var data    = temp[0];
            $('.dropdown').hide();
            $('#' + data).click();
        });

        //If bottom_separator hovered
        $('.item').click(function(){
            $('.dropdown').hide();
        });

        //If list of dropdown clicked, change the value 
        $('.dropdown p').click(function(){
            var value   = $(this).text();
            var tmp     = $(this).attr('for');
            $('#' + tmp).val(value);
            $('.dropdown').hide();
        });
        /* End
        ==================================*/
        /* === Button Lang and Nav ===*/
        $(".btn-lang").click(function(e) {
            $(".btn-lang").removeClass("active");
            $(this).addClass("active");
        });
        $(".navbar-nav li.menu").click(function(e) {
            $(".navbar-nav li.menu").removeClass("active");
            $(this).addClass("active");
        });
        /* === End Button Lang and Nav ===*/
        $('#owl-1').owlCarousel({
            loop: true,
            margin: 10,
            navText : ["",""],
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    loop: false,
                    nav: true
                },
                768: {
                    items: 2,
                    loop: false,
                    nav: false
                },
                1000: {
                    items: 3,
                    nav: true,
                    loop: false,
                    margin: 20
                }
            }
        });
    });
</script>