<?php

namespace App\Http\Controllers;

use App\About;
use App\Banner;
use App\Client;
use App\Collection;
use App\Content;
use App\Image;
use App\Menu;
use App\Privacy;
use App\Service;
use App\Socialmedia;
use App\Subscribe;
use App\Term;
use App\Vacancy;
use Illuminate\Http\Request;
use App\Contact;
use DB;
use View;

class WebController extends Controller
{

    public function __construct()
    {
        $fb = Socialmedia::whereTitle('Facebook')->where('language', 'eng')->first();
        $tw = Socialmedia::whereTitle('Twitter')->where('language', 'eng')->first();
        $ig = Socialmedia::whereTitle('Instagram')->where('language', 'eng')->first();
        $ta = Socialmedia::whereTitle('Tripadvisor')->where('language', 'eng')->first();
        $gp = Socialmedia::whereTitle('Google+')->where('language', 'eng')->first();

        $fb_id = Socialmedia::whereTitle('Facebook')->where('language', 'ind')->first();
        $tw_id = Socialmedia::whereTitle('Twitter')->where('language', 'ind')->first();
        $ig_id = Socialmedia::whereTitle('Instagram')->where('language', 'ind')->first();
        $ta_id = Socialmedia::whereTitle('Tripadvisor')->where('language', 'ind')->first();
        $gp_id = Socialmedia::whereTitle('Google+')->where('language', 'ind')->first();

        $contact = Contact::where('language', 'eng')->first();
        $contact_id = Contact::where('language', 'ind')->first();

        $count_en = $this->count_collection_en();

        $count_id = $this->count_collection_id();

        $collection = Collection::where('language', 'eng')->limit(5)->get();

        $collection_id = Collection::where('language', 'ind')->limit(5)->get();

        $w = $this->whatson_link();

        $w_id = $this->whatson_link_id();

        View::share('fb', $fb);
        View::share('tw', $tw);
        View::share('ig', $ig);
        View::share('ta', $ta);
        View::share('gp', $gp);
        View::share('w', $w);
        View::share('w_id', $w_id);

        View::share('fb_id', $fb_id);
        View::share('tw_id', $tw_id);
        View::share('ig_id', $ig_id);
        View::share('ta_id', $ta_id);
        View::share('gp_id', $gp_id);
        View::share('contact', $contact);
        View::share('contact_id', $contact_id);
        View::share('collection', $collection);
        View::share('collection_id', $collection_id);
        View::share('count_en', $count_en);
        View::share('count_id', $count_id);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getmenu = Menu ::whereTitle('homepage')->where('language', 'eng')->first();
        $menu_id = $getmenu['id'];

        $homepage = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'homepage')
            ->where('menu.language', 'eng')
            ->where('images.order', '!=', '0')
            ->get();

        return view('web.index', compact('homepage'));
    }

    public function index_id()
    {
        $getmenu = Menu ::whereTitle('homepage')->where('language', 'ind')->first();
        $menu_id = $getmenu['id'];

        $homepage = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'homepage')
            ->where('menu.language', 'ind')
            ->where('images.order', '!=', '0')
            ->get();

        return view('web.index', compact('homepage'));
    }

    public function room()
    {
        $getmenu = Menu ::whereTitle('room')->where('language', 'eng')->first();
        $menu_id = $getmenu['id'];

        $count = Content::where('menu_id', $menu_id)->count();

        $room = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'room')
            ->where('menu.language', 'eng')
            ->where('images.order', 0)
            ->get();

        return view('web.room', compact('room', 'count'));
    }

    public function room_id()
    {
        $getmenu = Menu ::whereTitle('room')->where('language', 'ind')->first();
        $menu_id = $getmenu['id'];

        $count = Content::where('menu_id', $menu_id)->count();

        $room = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'room')
            ->where('menu.language', 'ind')
            ->where('images.order', 0)
            ->get();

        return view('web.room', compact('room', 'count'));
    }

    public function meeting()
    {
        $getmenu = Menu ::whereTitle('meeting')->where('language', 'eng')->first();
        $menu_id = $getmenu['id'];

        $meeting = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'meeting')
            ->where('menu.language', 'eng')
            ->where('images.order', 0)
            ->get();

        return view('web.meeting', compact('meeting', 'image'));
    }

    public function meeting_id()
    {
        $getmenu = Menu ::whereTitle('meeting')->where('language', 'ind')->first();
        $menu_id = $getmenu['id'];

        $meeting = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'meeting')
            ->where('menu.language', 'ind')
            ->where('images.order', 0)
            ->get();

        return view('web.meeting', compact('meeting', 'image'));
    }

    public function facilities()
    {
        $getmenu = Menu ::whereTitle('facilities')->where('language', 'eng')->first();
        $menu_id = $getmenu['id'];

        $facilities = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'facilities')
            ->where('menu.language', 'eng')
            ->where('images.order', 0)
            ->orderBy('contents.order', 'ASC')
            ->get();

        return view('web.facilities', compact('facilities'));
    }

    public function facilities_id()
    {
        $getmenu = Menu ::whereTitle('facilities')->where('language', 'ind')->first();
        $menu_id = $getmenu['id'];

        $facilities = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'facilities')
            ->where('menu.language', 'ind')
            ->where('images.order', 0)
            ->orderBy('contents.order', 'ASC')
            ->get();

        return view('web.facilities', compact('facilities'));
    }

    public function gallery()
    {
        $getmenu = Menu ::whereTitle('gallery')->where('language', 'eng')->first();
        $menu_id = $getmenu['id'];

        $gallery = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'gallery')
            ->where('menu.language', 'eng')
            ->orderBy('contents.order', 'ASC')
            ->get();

        return view('web.gallery', compact('gallery'));
    }

    public function gallery_id()
    {
        $getmenu = Menu ::whereTitle('gallery')->where('language', 'ind')->first();
        $menu_id = $getmenu['id'];

        $gallery = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'gallery')
            ->where('menu.language', 'ind')
            ->orderBy('contents.order', 'ASC')
            ->get();

        return view('web.gallery', compact('gallery'));
    }

    public function neighbourhood()
    {
        $getmenu = Menu ::whereTitle('neighbourhood')->where('language', 'eng')->first();
        $menu_id = $getmenu['id'];

        $neighbourhood = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'neighbourhood')
            ->where('menu.language', 'eng')
            ->where('images.order', 0)
            ->get();

        return view ('web.neighbourhood', compact('neighbourhood'));
    }

    public function neighbourhood_id()
    {
        $getmenu = Menu ::whereTitle('neighbourhood')->where('language', 'ind')->first();
        $menu_id = $getmenu['id'];

        $neighbourhood = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.title', 'neighbourhood')
            ->where('menu.language', 'ind')
            ->where('images.order', 0)
            ->get();

        return view ('web.neighbourhood', compact('neighbourhood'));
    }

    public function whatson()
    {
        $count = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.parent_id', '!=', '0')
            ->where('menu.language', 'eng')
            ->where('images.order', 0)
            ->distinct('menu.title')
            ->count('menu.title');

        $whatson = Content::select('images.*', 'contents.*', 'menu.title as menu_title')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.parent_id', '!=', '0')
            ->where('menu.language', 'eng')
            ->where('images.order', 0)
            ->where('contents.order', 1)
            ->groupby('menu.title')
            ->get();

        return view ('web.whatson', compact('count', 'whatson'));
    }

    public function whatson_link()
    {
        $w = Content::select('images.*', 'contents.*', 'menu.title as menu_title')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.parent_id', '!=', '0')
            ->where('menu.language', 'eng')
            ->where('images.order', 0)
            ->groupby('menu.title')
            ->get();

        return $w;
    }

    public function whatson_link_id()
    {
        $w = Content::select('images.*', 'contents.*', 'menu.title as menu_title')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.parent_id', '!=', '0')
            ->where('menu.language', 'ind')
            ->where('images.order', 0)
            ->groupby('menu.title')
            ->get();

        return $w;
    }

    public function whatson_id()
    {
        $count = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.parent_id', '!=', '0')
            ->where('menu.language', 'ind')
            ->where('images.order', 0)
            ->distinct('menu.title')
            ->count('menu.title');

        $whatson = Content::select('images.*', 'contents.*', 'menu.title as menu_title')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.parent_id', '!=', '0')
            ->where('menu.language', 'ind')
            ->where('images.order', 0)
            ->where('contents.order', 1)
            ->groupby('menu.title')
            ->get();

        return view ('web.whatson', compact('count', 'whatson'));
    }

    public function detail($menu, $id)
    {
        $image = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'eng')
            ->where('contents.id', $id)
            ->where('images.order', '!=', '0')
            ->get();

        $images = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'eng')
            ->where('contents.id', $id)
            ->first();

        $detail = Content::select('images.*', 'contents.*', 'menu.title as menu_title')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'eng')
            ->where('contents.id', $id)
            ->first();

        $detail_hap = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'eng')
            ->where('menu.parent_id', '!=', '0')
            ->where('menu.title', $menu)
            ->orderBy('contents.order', 'ASC')
            ->get();

        $next = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'eng')
            ->where('menu.title', $menu)
            ->where('contents.id', '>', $id)
            ->where('contents.description', '<>', '')
            ->orderBy('contents.order', 'asc')
            ->first();

        $previous = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'eng')
            ->where('menu.title', $menu)
            ->where('contents.id', '<', $id)
            ->where('contents.description', '<>', '')
            ->orderBy('contents.order', 'desc')
            ->first();

        $max = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'eng')
            ->where('menu.title', $menu)
            ->where('contents.description', '<>', '')
            ->orderBy('contents.order', 'asc')
            ->max('contents.id');


        $min = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'eng')
            ->where('menu.title', $menu)
            ->where('contents.description', '<>', '')
            ->orderBy('contents.order', 'desc')
            ->min('contents.id');

        return view ('web.detail', compact('image', 'detail', 'images', 'detail_hap', 'menu', 'next', 'previous', 'min', 'max'));
    }

    public function detail_id($menu, $id)
    {
        $image = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'ind')
            ->where('contents.id', $id)
            ->where('images.order', '!=', '0')
            ->get();

        $images = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'ind')
            ->where('contents.id', $id)
            ->first();

        $detail = Content::select('images.*', 'contents.*', 'menu.title as menu_title')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'ind')
            ->where('contents.id', $id)
            ->first();

        $detail_hap = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'ind')
            ->where('menu.parent_id', '!=', '0')
            ->where('menu.title', $menu)
            ->orderBy('contents.order', 'ASC')
            ->get();

        $next = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'ind')
            ->where('menu.title', $menu)
            ->where('contents.id', '>', $id)
            ->where('contents.description', '<>', '')
            ->orderBy('contents.order', 'asc')
            ->first();

        $previous = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'ind')
            ->where('menu.title', $menu)
            ->where('contents.id', '<', $id)
            ->where('contents.description', '<>', '')
            ->orderBy('contents.order', 'desc')
            ->first();

        $max = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'ind')
            ->where('menu.title', $menu)
            ->where('contents.description', '<>', '')
            ->orderBy('contents.order', 'asc')
            ->max('contents.id');

        $min = Content::select('images.*', 'contents.*')
            ->leftJoin('menu','menu.id','=','contents.menu_id')
            ->leftJoin('images', 'images.content_id', '=', 'contents.id')
            ->where('menu.language', 'ind')
            ->where('menu.title', $menu)
            ->where('contents.description', '<>', '')
            ->orderBy('contents.order', 'desc')
            ->min('contents.id');

        return view ('web.detail', compact('image', 'detail', 'images', 'detail_hap', 'menu', 'next', 'previous', 'min', 'max'));
    }

    public function privacy()
    {
        $privacy = Privacy::where('language', 'eng')->first();

        return view('web.privacy', compact('privacy'));
    }

    public function privacy_id()
    {
        $privacy = Privacy::where('language', 'eng')->first();

        return view('web.privacy', compact('privacy'));
    }

    public function terms()
    {
        $terms = Term::where('language', 'eng')->first();

        return view('web.terms', compact('terms'));
    }

    public function terms_id()
    {
        $terms = Term::where('language', 'ind')->first();

        return view('web.terms', compact('terms'));
    }

    public function subscribe(Request $request)
    {
        $requestData = $request->all();

        Subscribe::create($requestData);

        return redirect('/');

    }

    public function count_collection_en(){

        $count_collection = Collection::where('language', 'eng')->count();

        return $count_collection;
    }

    public function count_collection_id(){

        $count_collection = Collection::where('language', 'id')->count();

        return $count_collection;
    }

}
