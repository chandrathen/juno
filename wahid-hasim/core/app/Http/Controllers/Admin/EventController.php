<?php

namespace App\Http\Controllers\Admin;

use App\Content;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Event;
use App\Image;
use App\Menu;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.event.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $lang = Session::get('lang');
        $getmenu = Menu ::whereTitle('event')->where('menu.language', $lang)->first();
        $menu_id = $getmenu['id'];

        return view('admin.event.create', compact('menu_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        Content::create($requestData);

        Session::flash('flash_message', 'Event added!');

        return redirect('admin/event');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $event = Content::findOrFail($id);

        return view('admin.event.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $event = Content::findOrFail($id);

        $image = Image::whereContentId($id)->orderBy('order', 'ASC')->get();

        return view('admin.event.edit', compact('event', 'image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();
        
        $event = Content::findOrFail($id);
        $event->update($requestData);

        Session::flash('flash_message', 'Event updated!');

        return redirect('admin/event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Content::destroy($id);

        Session::flash('flash_message', 'Event deleted!');

        return redirect('admin/event');
    }
    
    public function anyData(Request $request)
    {
        $lang = Session::get('lang');

        DB::statement(DB::raw('set @rownum=0'));
        $event = Content::join('menu','menu.id','=','contents.menu_id')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'contents.id', 'contents.title', 'contents.intro', 'contents.description'])->where('menu.title', 'event')
            ->where('menu.language', $lang)->orderBy('contents.order', 'ASC');
        
         $datatables = app('datatables')->of($event)
            ->addColumn('action', function ($event) {
                return '<a href="event/'.$event->id.'/edit" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'"><i class="fa fa-pencil"></i></a> <a onclick="deleteData('.$event->id.')" class="btn btn-xs btn-danger rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.delete') .'"><i class="fa fa-trash"></i></a>';
            });
        
        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }
        
        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);                                    
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);     
            }        
        }

        
        
        return $datatables->make(true);                                    
    }
}
