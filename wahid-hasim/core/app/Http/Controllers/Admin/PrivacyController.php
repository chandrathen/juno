<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Privacy;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class PrivacyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.privacy.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $lang = Session::get('lang');
        return view('admin.privacy.create', compact('lang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Privacy::create($requestData);

        Session::flash('flash_message', 'Privacy added!');

        return redirect('admin/privacy');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $privacy = Privacy::findOrFail($id);

        return view('admin.privacy.show', compact('privacy'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $privacy = Privacy::findOrFail($id);

        return view('admin.privacy.edit', compact('privacy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $privacy = Privacy::findOrFail($id);
        $privacy->update($requestData);

        Session::flash('flash_message', 'Privacy updated!');

        return redirect('admin/privacy');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Privacy::destroy($id);

        Session::flash('flash_message', 'Privacy deleted!');

        return redirect('admin/privacy');
    }
    
    public function anyData(Request $request)
    {
        $lang = Session::get('lang');

        DB::statement(DB::raw('set @rownum=0'));
        $privacy = Privacy::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'title', 'description'])->where('language', $lang);
        
         $datatables = app('datatables')->of($privacy)
            ->addColumn('action', function ($privacy) {
                return '<a href="privacy/'.$privacy->id.'/edit" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'"><i class="fa fa-pencil"></i></a> ';
            });
        
        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }
        
        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);                                    
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);     
            }        
        }

        
        
        return $datatables->make(true);                                    
    }
}
