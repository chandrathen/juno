<?php

Route::get('/admin', function () {
    return redirect('login');
});


Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/admin', 'AdminController@index')->name('admin');

Route::post('admin/upload', ['as' => 'upload', 'uses' => 'Admin\\ImageController@upload']);
Route::get('admin/upload/delete/{id}', ['as' => 'upload', 'uses' => 'Admin\\ImageController@destroy']);

## USER ##
Route::get('admin/user/data', ['as' => 'user.data', 'uses' => 'Admin\\UserController@anyData']);
Route::resource('admin/user', 'Admin\\UserController', [
    'names' => [
        'index' => 'user',
        'create' => 'user.new',
        'edit' => 'user.edit',
        'show' => 'user.show',
    ]
]);

### HOMEPAGE ###
Route::get('admin/homepage/data', ['as' => 'homepage.data', 'uses' => 'Admin\\HomepageController@anyData']);
Route::resource('admin/homepage', 'Admin\\HomepageController', [
    'names' => [
        'index' => 'homepage',
        'create' => 'homepage.new',
        'edit' => 'homepage.edit',
        'show' => 'homepage.show',
    ]
]);

### ROOM ###
Route::get('admin/room/data', ['as' => 'room.data', 'uses' => 'Admin\\RoomController@anyData']);
Route::resource('admin/room', 'Admin\\RoomController', [
    'names' => [
        'index' => 'room',
        'create' => 'room.new',
        'edit' => 'room.edit',
        'show' => 'room.show',
    ]
]);

### MEETING ###
Route::get('admin/meeting/data', ['as' => 'meeting.data', 'uses' => 'Admin\\MeetingController@anyData']);
Route::resource('admin/meeting', 'Admin\\MeetingController', [
    'names' => [
        'index' => 'meeting',
        'create' => 'meeting.new',
        'edit' => 'meeting.edit',
        'show' => 'meeting.show',
    ]
]);

### FACILITIES ###
Route::get('admin/facilities/data', ['as' => 'facilities.data', 'uses' => 'Admin\\FacilitiesController@anyData']);
Route::resource('admin/facilities', 'Admin\\FacilitiesController', [
    'names' => [
        'index' => 'facilities',
        'create' => 'facilities.new',
        'edit' => 'facilities.edit',
        'show' => 'facilities.show',
    ]
]);

### GALLERY ###
Route::get('admin/gallery/data', ['as' => 'gallery.data', 'uses' => 'Admin\\GalleryController@anyData']);
Route::resource('admin/gallery', 'Admin\\GalleryController', [
    'names' => [
        'index' => 'gallery',
        'create' => 'gallery.new',
        'edit' => 'gallery.edit',
        'show' => 'gallery.show',
    ]
]);

### NEIGHBOURHOOD ###
Route::get('admin/neighbourhood/data', ['as' => 'neighbourhood.data', 'uses' => 'Admin\\NeighbourhoodController@anyData']);
Route::resource('admin/neighbourhood', 'Admin\\NeighbourhoodController', [
    'names' => [
        'index' => 'neighbourhood',
        'create' => 'neighbourhood.new',
        'edit' => 'neighbourhood.edit',
        'show' => 'neighbourhood.show',
    ]
]);

### WHATSON ###
Route::get('admin/whatson/data', ['as' => 'whatson.data', 'uses' => 'Admin\\WhatsonController@anyData']);
Route::resource('admin/whatson', 'Admin\\WhatsonController', [
    'names' => [
        'index' => 'whatson',
        'create' => 'whatson.new',
        'edit' => 'whatson.edit',
        'show' => 'whatson.show',
    ]
]);

### EVENT ###
Route::get('admin/event/data', ['as' => 'event.data', 'uses' => 'Admin\\EventController@anyData']);
Route::resource('admin/event', 'Admin\\EventController', [
    'names' => [
        'index' => 'event',
        'create' => 'event.new',
        'edit' => 'event.edit',
        'show' => 'event.show',
    ]
]);

### HAPPENING ###
Route::get('admin/happening/data', ['as' => 'happening.data', 'uses' => 'Admin\\HappeningController@anyData']);
Route::resource('admin/happening', 'Admin\\HappeningController', [
    'names' => [
        'index' => 'happening',
        'create' => 'happening.new',
        'edit' => 'happening.edit',
        'show' => 'happening.show',
    ]
]);

### SOCIAL MEDIA ###
Route::get('admin/socialmedia/data', ['as' => 'socialmedia.data', 'uses' => 'Admin\\SocialmediaController@anyData']);
Route::resource('admin/socialmedia', 'Admin\\SocialmediaController', [
    'names' => [
        'index' => 'socialmedia',
        'create' => 'socialmedia.new',
        'edit' => 'socialmedia.edit',
        'show' => 'socialmedia.show',
    ]
]);

### CONTACT ###
Route::get('admin/contact/data', ['as' => 'contact.data', 'uses' => 'Admin\\ContactController@anyData']);
Route::resource('admin/contact', 'Admin\\ContactController', [
    'names' => [
        'index' => 'contact',
        'create' => 'contact.new',
        'edit' => 'contact.edit',
        'show' => 'contact.show',
    ]
]);

### PRIVACY ###
Route::get('admin/privacy/data', ['as' => 'privacy.data', 'uses' => 'Admin\\PrivacyController@anyData']);
Route::resource('admin/privacy', 'Admin\\PrivacyController', [
    'names' => [
        'index' => 'privacy',
        'create' => 'privacy.new',
        'edit' => 'privacy.edit',
        'show' => 'privacy.show',
    ]
]);


### TERMS CONDITION ###
Route::get('admin/terms/data', ['as' => 'terms.data', 'uses' => 'Admin\\TermsController@anyData']);
Route::resource('admin/terms', 'Admin\\TermsController', [
    'names' => [
        'index' => 'terms',
        'create' => 'terms.new',
        'edit' => 'terms.edit',
        'show' => 'terms.show',
    ]
]);

### SUBSCRIBE ####
Route::get('admin/subscribe/data', ['as' => 'subscribe.data', 'uses' => 'Admin\\SubscribeController@anyData']);
Route::resource('admin/subscribe', 'Admin\\SubscribeController', [
    'names' => [
        'index' => 'subscribe',
        'create' => 'subscribe.new',
        'edit' => 'subscribe.edit',
        'show' => 'subscribe.show',
    ]
]);


## COLLECTION ###
Route::get('admin/collection/data', ['as' => 'collection.data', 'uses' => 'Admin\\CollectionController@anyData']);
Route::resource('admin/collection', 'Admin\\CollectionController', [
    'names' => [
        'index' => 'collection',
        'create' => 'collection.new',
        'edit' => 'collection.edit',
        'show' => 'collection.show',
    ]
]);
Route::get('/changelang/', 'AdminController@changelang');


### WEB ###
Route::get('/', function () {
    return redirect('/en');
});

Route::get('/en', 'WebController@index')->name('web_en');
Route::get('/id', 'WebController@index_id')->name('web_id');

Route::get('/en/room', 'WebController@room')->name('room_en');
Route::get('/id/room', 'WebController@room_id')->name('room_id');

Route::get('/en/meeting', 'WebController@meeting')->name('meeting_en');
Route::get('/id/meeting', 'WebController@meeting_id')->name('meeting_id');

Route::get('/en/facilities', 'WebController@facilities')->name('facilities_en');
Route::get('/id/facilities', 'WebController@facilities_id')->name('facilities_id');

Route::get('/en/gallery', 'WebController@gallery')->name('gallery_en');
Route::get('/id/gallery', 'WebController@gallery_id')->name('gallery_id');

Route::get('/en/neighbourhood', 'WebController@neighbourhood')->name('neighbourhood_en');
Route::get('/id/neighbourhood', 'WebController@neighbourhood_id')->name('neighbourhood_id');

Route::get('/en/whatson', 'WebController@whatson')->name('whatson_en');
Route::get('/id/whatson', 'WebController@whatson_id')->name('whatson_id');

Route::get('/en/privacy-policy', 'WebController@privacy')->name('privacy_en');
Route::get('/id/privacy-policy', 'WebController@privacy_id')->name('privacy_id');

Route::get('/en/terms-conditions', 'WebController@terms')->name('terms_en');
Route::get('/id/terms-conditions', 'WebController@terms_id')->name('terms_id');

Route::get('/en/{menu}/detail/{id}', 'WebController@detail')->name('detail_en');
Route::get('/id/{menu}/detail/{id}', 'WebController@detail_id')->name('detail_id');


Route::post('/subscribe', 'WebController@subscribe')->name('subscribes');
