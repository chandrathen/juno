@extends('layouts.web.frame')

@section('content')

    <div id="content_container">
        <div id="loader"></div>
        <div style="display:none;" id="myLoader" class="animate-bottom">
            <section class="slider">
                <div id="slider" class="flexslider">
                    <ul class="slides">
                        @foreach($gallery as $index => $val)
                        <li>
                            <img src="{{ url('/uploads/'.$val->image) }}" />
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div id="carousel" class="flexslider">
                    <ul class="slides">
                        @foreach($gallery as $index => $val)
                        <li>
                            <img src="{{ url('/uploads/' .$val->thumbnail) }}" />
                        </li>
                        @endforeach
                    </ul>
                </div>
            </section>
        </div>
    </div>

@endsection