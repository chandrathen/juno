@extends('layouts.web.frame')

@section('content')

    <div id="content_terms">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h1 class="title">{{ $terms->title }}</h1>
                    {!! $terms->description !!}
                </div>
            </div>
        </div>
    </div>

@endsection