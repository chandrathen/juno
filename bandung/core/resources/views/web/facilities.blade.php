@extends('layouts.web.frame')

@section('content')

    <div id="content_container" class="facilities-container">
        <div id="content_wrapper" class="container-fluid">
            <div id="content" class="facilities">
                <div class="row">
                    @foreach($facilities as $index => $val)
                    <div class="col-md-3 col-sm-6 content_box hover_bw_color">
                        <div class="image_cover" style="background-image: url({{ url('/uploads/'. $val->image)}}); background-size: cover; background-position: center; height: 280px;"></div>
                        <div class="content_box_name facilities_title">{{ $val->title }}</div>
                        <div class="content_box_description">
                            {!! html_entity_decode($val->description) !!}
                        </div>
                        <div class="content_box_book">
                            @if(Request::segment(1) == 'en')
                                <a href="{{ url('/en/facilities/detail/'. $val->id) }}">
                            @elseif(Request::segment(1) == 'id')
                                <a href="{{ url('/id/facilities/detail/'. $val->id) }}">
                            @endif

                            @if($val->hidden_btn_title == 0)
                                <div class="content_box_book_button">{{ $val->btn_title }}</div>
                            @endif
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection