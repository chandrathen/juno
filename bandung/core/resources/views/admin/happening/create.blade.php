@extends('layouts.app.frame')
@section('title', 'Create New Happening')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('happening.new'))
@section('button', '<a href="'.url('/admin/happening').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')

    {!! Form::open(['url' => '/admin/happening', 'id' => 'formValidate', 'files' => true, 'enctype' => 'multipart/form-data']) !!}
        		
		@include ('admin.happening.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});

tinymce.init({
    selector:'textarea',
    plugins: "code",
    menubar: "file edit insert view format table tools",
    height : 100
});
</script>
@endpush