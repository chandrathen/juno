@extends('layouts.app.frame')
@section('title', 'Edit Gallery')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('gallery.edit', $gallery))
@section('button', '<a href="'.url('/admin/gallery').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')

    <div id="modBanner" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Banner</h4>
                </div>
                <form onsubmit="return validate()" action="{{url('admin/upload')}}" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group form-group-default">
                            <label>File</label>
                            <input id="files" type="file" class="form-control" name="files" />
                            <p class="error small">Dimensions: 1349px x 637px, Max size: 2MB</p>
                            <input hidden name="_token" value="{{csrf_token()}}" />
                            @if(isset($gallery))
                                <input type="hidden" name="content_id" id="content_id" value="{{ $gallery->id }}">
                            @endif
                            <label>Order</label>
                            <input type="number" name="order" id="order" min="1">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <form class="row" role="form">
        <div class="col-md-12">
            <div class="form-group form-group-default">
                <label>Banner</label>
                @foreach($image as $index => $val)
                    <div class="col-md-3 pb">
                        <a class="fancybox" rel="group" href="{{url('/')}}/uploads/{{$val->image}}" id="{{$val->id}}"><img src="{{url('/')}}/uploads/{{$val->image}}" style="max-width:100%; border-radius: 20px;" alt="" /></a>
                        <label>Order</label>
                        <input type="text" value="{{ $val->order }}" disabled>
                    </div>
                @endforeach
                <div class="col-md-12">
                    <a data-toggle="modal" data-target="#modBanner" class="btn btn-sm btn-primary" style="margin-top:10px;"><i class="fa fa-plus"></i> Add Banner</a>
                </div>
            </div>
        </div>
    </form>

    {!! Form::model($gallery, [
            'method' => 'PATCH',
            'url' => ['/admin/gallery', $gallery->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}
        				
        @include ('admin.gallery.form', ['submitButtonText' => 'Update'])
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});

$(document).ready(function() {
    $(".fancybox").each(function(){
        $(this).fancybox({
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'speedIn': 600,
            'speedOut': 200,
            'title': '<a href="javascript:;" onclick="return remove('+this.id+')"><i class="fa fa-times"></i> Remove</a>'
        });
    });
});

function remove(id)
{
    var conf = confirm('Hapus Gambar Ini?');
    if(conf){
        $.ajax({
            url: "{{url('admin/upload/delete')}}"+"/"+id,
            type: 'GET',
            success: window.location.href="{{ url('admin/gallery/'.$gallery->id.'/edit')}}"
        });
    }
}

function validate()
{
    var fileName = $('#files').val();
    var allowed_extensions = new Array("jpeg","jpg","png","gif");
    var file_extension = fileName.split('.').pop(); // split function will split the filename by dot(.), and pop function will pop the last element from the array which will give you the extension as well. If there will be no extension then it will return the filename.

    for(var i = 0; i <= allowed_extensions.length; i++)
    {
        if(allowed_extensions[i]==file_extension)
        {
            return true; // valid file extension
        }
    }

    alert('File harus berupa Gambar!');
    return false;
}

tinymce.init({
    selector:'textarea',
    plugins: "code",
    menubar: "file edit insert view format table tools",
    height : 100
});

tinymce.init({
    selector:'textarea',
    plugins: "code",
    menubar: "file edit insert view format table tools",
    height : 100
});
</script>
@endpush