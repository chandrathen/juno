@extends('layouts.app.frame')
@section('title', 'Create New Neighbourhood')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('neighbourhood.new'))
@section('button', '<a href="'.url('/admin/neighbourhood').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/neighbourhood', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.neighbourhood.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});

tinymce.init({
    selector:'textarea',
    plugins: "code",
    menubar: "file edit insert view format table tools",
    height : 100
});
</script>
@endpush