@extends('layouts.app.frame')
@section('title', 'Edit Term')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('terms.edit', $term))
@section('button', '<a href="'.url('/admin/terms').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::model($term, [
            'method' => 'PATCH',
            'url' => ['/admin/terms', $term->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}
        				
        @include ('admin.terms.form', ['submitButtonText' => 'Update'])
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
tinymce.init({
    selector:'textarea',
    plugins: "code",
    menubar: "file edit insert view format table tools",
    height : 100
});
</script>
@endpush