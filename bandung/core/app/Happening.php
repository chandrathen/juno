<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Happening extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'happenings';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'intro', 'description', 'order', 'btn_title', 'link_types', 'link', 'menu_id'];

    
}
