<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Image;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Thumbs;
use Session;
use Datatables;
use DB;
use Auth;
use Input;
use Validator;
use File;
use Response;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.image.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.image.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Image::create($requestData);

        Session::flash('flash_message', 'Image added!');

        return redirect('admin/image');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function upload(Request $request)
    {
        $this->validate($request, [
            'files' => 'required|max:20000|mimes:jpg,png,jpeg',
        ]);

        if($request->hasFile('files')){
            $file = $request->file('files');
            $ext = $file->getClientOriginalExtension();
            $name = rand(10000,99999).'.'.$ext;
            $path = 'uploads/';
            $file->move($path,$name);
            $img = $path.$name;
            $thumb = $path.'thumb_'.$name;
            $thumbs = 'thumb_'.$name;
            File::copy($img,$thumb);
            Thumbs::make($thumb)->resize(318, 188)->save();
            $data['image'] = $name;
            $data['content_id'] = $request->content_id;
            $data['order'] = $request->order;
            $data['thumbnail'] = $thumbs;

            Image::create($data);
        }

        return back();
    }

    public function show($id)
    {
        $image = Image::findOrFail($id);

        return view('admin.image.show', compact('image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $image = Image::findOrFail($id);

        return view('admin.image.edit', compact('image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $image = Image::findOrFail($id);
        $image->update($requestData);

        Session::flash('flash_message', 'Image updated!');

        return redirect('admin/image');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
//        return $id;
        $image = Image::destroy($id);
        File::delete($image->image);

        Session::flash('flash_message', 'Image deleted!');

    }
    
    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $image = Image::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'name']);
        
         $datatables = app('datatables')->of($image)
            ->addColumn('action', function ($image) {
                return '<a href="image/'.$image->id.'/edit" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'"><i class="fa fa-pencil"></i></a> <a onclick="deleteData('.$image->id.')" class="btn btn-xs btn-danger rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.delete') .'"><i class="fa fa-trash"></i></a>';
            });
        
        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }
        
        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);                                    
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);     
            }        
        }

        
        
        return $datatables->make(true);                                    
    }
}
