@extends('layouts.app.frame')
@section('title', 'Edit Image')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('image.edit', $image))
@section('button', '<a href="'.url('/admin/image').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::model($image, [
            'method' => 'PATCH',
            'url' => ['/admin/image', $image->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}
        				
        @include ('admin.image.form', ['submitButtonText' => 'Update'])
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush