@extends('layouts.app.frame')
@section('title', 'Create New Subscribe')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('subscribe.new'))
@section('button', '<a href="'.url('/admin/subscribe').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/subscribe', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.subscribe.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush