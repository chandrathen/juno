@extends('layouts.app.frame')
@section('title', 'Create New Event')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('event.new'))
@section('button', '<a href="'.url('/admin/event').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/event', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.event.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});

tinymce.init({
    selector:'textarea',
    plugins: "code",
    menubar: "file edit insert view format table tools",
    height : 100
});
</script>
@endpush