@extends('layouts.app.frame')
@section('title', 'Edit Socialmedia')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('socialmedia.edit', $socialmedia))
@section('button', '<a href="'.url('/admin/socialmedia').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::model($socialmedia, [
            'method' => 'PATCH',
            'url' => ['/admin/socialmedia', $socialmedia->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}
        				
        @include ('admin.socialmedia.form', ['submitButtonText' => 'Update'])
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush