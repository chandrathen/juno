@extends('layouts.app.frame')
@section('title', 'Create New Socialmedia')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('socialmedia.new'))
@section('button', '<a href="'.url('/admin/socialmedia').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/socialmedia', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.socialmedia.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush