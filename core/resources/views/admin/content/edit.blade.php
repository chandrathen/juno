@extends('layouts.app.frame')
@section('title', 'Edit Content')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('content.edit', $content))
@section('button', '<a href="'.url('/admin/content').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::model($content, [
            'method' => 'PATCH',
            'url' => ['/admin/content', $content->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}
        				
        @include ('admin.content.form', ['submitButtonText' => 'Update'])
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush