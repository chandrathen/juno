<div aria-required="true" class="form-group form-group-default required {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('title', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('intro') ? 'has-error' : ''}}">
    {!! Form::label('intro', 'Intro') !!}
    {!! Form::textarea('intro', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('intro', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('description', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('order') ? 'has-error' : ''}}">
    {!! Form::label('order', 'Order') !!}
    {!! Form::number('order', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('order', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('btn_title') ? 'has-error' : ''}}">
    {!! Form::label('btn_title', 'Btn Title') !!}
    {!! Form::text('btn_title', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('btn_title', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('link_types') ? 'has-error' : ''}}">
    {!! Form::label('link_types', 'Link Types') !!}
    {!! Form::text('link_types', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('link_types', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('link') ? 'has-error' : ''}}">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('link', '<label class="error">:message</label>') !!}   
<div aria-required="true" class="form-group form-group-default required {{ $errors->has('menu_id') ? 'has-error' : ''}}">
    {!! Form::label('menu_id', 'Menu Id') !!}
    {!! Form::number('menu_id', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('menu_id', '<label class="error">:message</label>') !!}   


<div class="pull-left">
    <div class="checkbox check-success">
        <input id="checkbox-agree" type="checkbox" required> <label for="checkbox-agree">Saya sudah mengecek data sebelum menyimpan</label>
    </div>
</div>
<br/>

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<button class="btn btn-default" type="reset"><i class="pg-close"></i> Clear</button>
