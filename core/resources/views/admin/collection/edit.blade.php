@extends('layouts.app.frame')
@section('title', 'Edit Collection')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('collection.edit', $collection))
@section('button', '<a href="'.url('/admin/collection').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::model($collection, [
            'method' => 'PATCH',
            'url' => ['/admin/collection', $collection->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}
        				
        @include ('admin.collection.form', ['submitButtonText' => 'Update'])
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush