@extends('layouts.app.frame')
@section('title', 'Create New Term')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('terms.new'))
@section('button', '<a href="'.url('/admin/terms').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/terms', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.terms.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
tinymce.init({
    selector:'textarea',
    plugins: "code",
    menubar: "file edit insert view format table tools",
    height : 100
});
</script>
@endpush