@extends('layouts.app.frame')
@section('title', 'Create New Meeting')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('meeting.new'))
@section('button', '<a href="'.url('/admin/meeting').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/meeting', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.meeting.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});

tinymce.init({
    selector:'textarea',
    plugins: "code",
    menubar: "file edit insert view format table tools",
    height : 100
});
</script>
@endpush