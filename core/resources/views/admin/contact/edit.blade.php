@extends('layouts.app.frame')
@section('title', 'Edit Contact')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('contact.edit', $contact))
@section('button', '<a href="'.url('/admin/contact').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::model($contact, [
            'method' => 'PATCH',
            'url' => ['/admin/contact', $contact->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}
        				
        @include ('admin.contact.form', ['submitButtonText' => 'Update'])
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});

tinymce.init({
    selector:'textarea',
    plugins: "code",
    menubar: "file edit insert view format table tools",
    height : 100
});
</script>
@endpush