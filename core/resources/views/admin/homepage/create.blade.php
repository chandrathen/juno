@extends('layouts.app.frame')
@section('title', 'Create New Homepage')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('homepage.new'))
@section('button', '<a href="'.url('/admin/homepage').'" class="btn btn-info btn-xs no-border">Back 
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/homepage', 'id' => 'formValidate', 'files' => true]) !!}
        		
		@include ('admin.homepage.form')
		
	{!! Form::close() !!}
@endsection


@push("script")
<script>
$(document).ready(function() {
    
    $('#formValidate').validate();
      
});
</script>
@endpush