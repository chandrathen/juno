@extends('layouts.web.frame')

@section('content')
<div id="content_container" class="clear-min-height">
    <div class="wide-container">
        <div id="slides2">
            <ul class="slides-container">
                @foreach($meeting as $index => $val)
                    <li>
                        <img src="{{ url('/uploads/'. $val->image) }}">
                        <div class="black-back-opacity">
                            <div class="fly-content">
                                <div class="detail-title">
                                    <h1>Test</h1>
                                </div>
                                <div class="desc_detail">
                                    <p>Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus suscipit tortor eget felis porttitor volutpat.</p>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>