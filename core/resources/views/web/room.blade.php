@extends('layouts.web.frame')

@section('content')

    <div id="content_container">
        <!-- 3 Colomn -->
        @if($count >= 3)
        <div id="content_wrapper" class="container-fluid room-content-container">
            <div id="content">
                <div class="row">
                    <div class="owl-carousel owl-theme">
                        @foreach($room as $index => $val)
                        <div class="col-md-12 col-sm-12 content_box hover_bw_color">
                            <div class="image_cover" style="background-image: url({{ url('/uploads/'. $val->image)}}); background-size: cover; background-position: center; height: 280px;"></div>
                            <div class="content_box_name">{{ $val->title }}</div>
                            <div class="content_box_description">
                                {!! html_entity_decode($val->intro) !!}
                            </div>
                            <div class="content_box_book">
                                @if(Request::segment(1) == 'en')
                                    <a href="{{ url('/en/room/detail/'. $val->id) }}">
                                @elseif(Request::segment(1) == 'id')
                                    <a href="{{ url('/id/room/detail/'. $val->id) }}">
                                @endif
                                    <div class="content_box_book_button">{{ $val->btn_title }}</div>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        @elseif( $count == 2)
    <!-- 2 Colomn -->
        <div id="content_wrapper" class="container-fluid room-content-container">
            <div id="content" class="col-md-8 col-md-offset-2">
                <div class="row">
                    @foreach($room as $index => $val)
                    <div class="col-md-6 col-sm-6 content_box hover_bw_color">
                        <div class="image_cover" style="background-image: url({{ url('/uploads/'. $val->image)}}); background-size: cover; background-position: center; height: 280px;"></div>
                        <div class="content_box_name">{{ $val->title }}</div>
                        <div class="content_box_description">
                            {!! html_entity_decode($val->intro) !!}
                        </div>
                        <div class="content_box_book">
                            @if(Request::segment(1) == 'en')
                                <a href="{{ url('/en/room/detail/'. $val->id) }}">
                            @elseif(Request::segment(1) == 'id')
                                <a href="{{ url('/id/room/detail/'. $val->id) }}">
                            @endif
                                <div class="content_box_book_button">{{ $val->btn_title }}</div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @else

        <div id="content_wrapper" class="container-fluid">
            <div id="content" class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div class="row">
                    @foreach($room as $index => $val)
                    <div class="col-md-12 col-sm-12 content_box hover_bw_color">
                        <div class="image_cover" style="background-image: url({{ url('/uploads/'. $val->image)}}); background-size: cover; background-position: center; height: 280px;"></div>
                        <div class="content_box_name">{{ $val->title }}</div>
                        <div class="content_box_description">
                            {!! html_entity_decode($val->intro) !!}
                        </div>
                        <div class="content_box_book">
                            @if(Request::segment(1) == 'en')
                                <a href="{{ url('/en/room/detail/'. $val->id) }}">
                            @elseif(Request::segment(1) == 'id')
                                <a href="{{ url('/id/room/detail/'. $val->id) }}">
                            @endif
                                <div class="content_box_book_button">{{ $val->btn_title }}</div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
    </div>

@endsection