<link href="{{ asset('plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/bootstrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ asset('plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ asset('plugins/nvd3/nv.d3.min.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ asset('plugins/mapplic/css/mapplic.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/rickshaw/rickshaw.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ asset('plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('plugins/jquery-datatable/extensions/Buttons/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('plugins/jquery-datatable/extensions/Buttons/css/buttons.bootstrap.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('plugins/datatables-responsive/css/datatables.responsive.css') }}" media="screen" type="text/css" rel="stylesheet"/>
<link href="{{ asset('plugins/jquery-metrojs/MetroJs.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ asset('css/pages-iconss.css') }}" rel="stylesheet" type="text/css"/>
<link class="main-stylesheet" href="{{ asset('css/pages.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/aldesign.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/jquery-datatable/extensions/FixedHeader/css/dataTables.fixedHeader.min.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('css/dropzone.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ asset('css/basic.css') }}" type="text/css" rel="stylesheet"/>
<script src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>


<!--[if lte IE 9]>
    <link href="{{ asset('plugins/codrops-dialogFx/dialog.ie.css') }}" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->

<script>
    window.onload = function()
    {
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    };

    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>