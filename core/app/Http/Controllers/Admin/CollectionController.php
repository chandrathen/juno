<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Collection;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class CollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.collection.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $lang = Session::get('lang');
        return view('admin.collection.create', compact('lang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Collection::create($requestData);

        Session::flash('flash_message', 'Collection added!');

        return redirect('admin/collection');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $collection = Collection::findOrFail($id);

        return view('admin.collection.show', compact('collection'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $collection = Collection::findOrFail($id);

        return view('admin.collection.edit', compact('collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $collection = Collection::findOrFail($id);
        $collection->update($requestData);

        Session::flash('flash_message', 'Collection updated!');

        return redirect('admin/collection');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Collection::destroy($id);

        Session::flash('flash_message', 'Collection deleted!');

        return redirect('admin/collection');
    }
    
    public function anyData(Request $request)
    {
        $lang = Session::get('lang');

        DB::statement(DB::raw('set @rownum=0'));
        $collection = Collection::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'name', 'value'])->where('language', $lang);
        
         $datatables = app('datatables')->of($collection)
            ->addColumn('action', function ($collection) {
                return '<a href="collection/'.$collection->id.'/edit" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'"><i class="fa fa-pencil"></i></a> <a onclick="deleteData('.$collection->id.')" class="btn btn-xs btn-danger rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.delete') .'"><i class="fa fa-trash"></i></a>';
            });
        
        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }
        
        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);                                    
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);     
            }        
        }

        
        
        return $datatables->make(true);                                    
    }
}
