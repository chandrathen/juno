<?php

Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('admin'));
});

## USER ##
Breadcrumbs::register('user', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('User', route('user'));
});
Breadcrumbs::register('user.new', function($breadcrumbs)
{
    $breadcrumbs->parent('user');
    $breadcrumbs->push('New', route('user.new'));
});
Breadcrumbs::register('user.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('user');
    $breadcrumbs->push('Edit', route('user.edit', $data->id));
});

## HOMEPAGE ##
Breadcrumbs::register('homepage', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Homepage', route('homepage'));
});
Breadcrumbs::register('homepage.new', function($breadcrumbs)
{
    $breadcrumbs->parent('homepage');
    $breadcrumbs->push('New', route('homepage.new'));
});
Breadcrumbs::register('homepage.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('homepage');
    $breadcrumbs->push('Edit', route('homepage.edit', $data->id));
});

## ROOM ##
Breadcrumbs::register('room', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Room', route('room'));
});
Breadcrumbs::register('room.new', function($breadcrumbs)
{
    $breadcrumbs->parent('room');
    $breadcrumbs->push('New', route('room.new'));
});
Breadcrumbs::register('room.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('room');
    $breadcrumbs->push('Edit', route('room.edit', $data->id));
});

## MEETING ##
Breadcrumbs::register('meeting', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Meeting', route('meeting'));
});
Breadcrumbs::register('meeting.new', function($breadcrumbs)
{
    $breadcrumbs->parent('meeting');
    $breadcrumbs->push('New', route('meeting.new'));
});
Breadcrumbs::register('meeting.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('meeting');
    $breadcrumbs->push('Edit', route('meeting.edit', $data->id));
});

## FACILITIES ##
Breadcrumbs::register('facilities', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Facilities', route('facilities'));
});
Breadcrumbs::register('facilities.new', function($breadcrumbs)
{
    $breadcrumbs->parent('facilities');
    $breadcrumbs->push('New', route('facilities.new'));
});
Breadcrumbs::register('facilities.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('facilities');
    $breadcrumbs->push('Edit', route('facilities.edit', $data->id));
});

## GALLERY ##
Breadcrumbs::register('gallery', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Gallery', route('gallery'));
});
Breadcrumbs::register('gallery.new', function($breadcrumbs)
{
    $breadcrumbs->parent('gallery');
    $breadcrumbs->push('New', route('gallery.new'));
});
Breadcrumbs::register('gallery.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('gallery');
    $breadcrumbs->push('Edit', route('gallery.edit', $data->id));
});

## NEIGHBOURHOOD ##
Breadcrumbs::register('neighbourhood', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Neighbourhood', route('neighbourhood'));
});
Breadcrumbs::register('neighbourhood.new', function($breadcrumbs)
{
    $breadcrumbs->parent('neighbourhood');
    $breadcrumbs->push('New', route('neighbourhood.new'));
});
Breadcrumbs::register('neighbourhood.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('neighbourhood');
    $breadcrumbs->push('Edit', route('neighbourhood.edit', $data->id));
});

## WHATSON ##
Breadcrumbs::register('whatson', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Whatson', route('whatson'));
});
Breadcrumbs::register('whatson.new', function($breadcrumbs)
{
    $breadcrumbs->parent('whatson');
    $breadcrumbs->push('New', route('whatson.new'));
});
Breadcrumbs::register('whatson.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('whatson');
    $breadcrumbs->push('Edit', route('whatson.edit', $data->id));
});

## EVENT ##
Breadcrumbs::register('event', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Event', route('event'));
});
Breadcrumbs::register('event.new', function($breadcrumbs)
{
    $breadcrumbs->parent('event');
    $breadcrumbs->push('New', route('event.new'));
});
Breadcrumbs::register('event.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('event');
    $breadcrumbs->push('Edit', route('event.edit', $data->id));
});

## HAPPENING ##
Breadcrumbs::register('happening', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Happening', route('happening'));
});
Breadcrumbs::register('happening.new', function($breadcrumbs)
{
    $breadcrumbs->parent('happening');
    $breadcrumbs->push('New', route('happening.new'));
});
Breadcrumbs::register('happening.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('happening');
    $breadcrumbs->push('Edit', route('happening.edit', $data->id));
});


## SOCIAL MEDIA ##
Breadcrumbs::register('socialmedia', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Social Media', route('socialmedia'));
});
Breadcrumbs::register('socialmedia.new', function($breadcrumbs)
{
    $breadcrumbs->parent('socialmedia');
    $breadcrumbs->push('New', route('socialmedia.new'));
});
Breadcrumbs::register('socialmedia.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('socialmedia');
    $breadcrumbs->push('Edit', route('socialmedia.edit', $data->id));
});


## CONTACTS ##
Breadcrumbs::register('contact', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Contact', route('contact'));
});
Breadcrumbs::register('contact.new', function($breadcrumbs)
{
    $breadcrumbs->parent('contact');
    $breadcrumbs->push('New', route('contact.new'));
});
Breadcrumbs::register('contact.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('contact');
    $breadcrumbs->push('Edit', route('contact.edit', $data->id));
});


## PRIVACY ##
Breadcrumbs::register('privacy', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Privacy Policy', route('privacy'));
});
Breadcrumbs::register('privacy.new', function($breadcrumbs)
{
    $breadcrumbs->parent('privacy');
    $breadcrumbs->push('New', route('privacy.new'));
});
Breadcrumbs::register('privacy.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('privacy');
    $breadcrumbs->push('Edit', route('privacy.edit', $data->id));
});


## TERMS CONDITION ##
Breadcrumbs::register('terms', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Terms & Condition', route('terms'));
});
Breadcrumbs::register('terms.new', function($breadcrumbs)
{
    $breadcrumbs->parent('terms');
    $breadcrumbs->push('New', route('terms.new'));
});
Breadcrumbs::register('terms.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('terms');
    $breadcrumbs->push('Edit', route('terms.edit', $data->id));
});


## SUBSCRIPTIONS ##
Breadcrumbs::register('subscribe', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Subscriptions', route('subscribe'));
});
Breadcrumbs::register('subscribe.new', function($breadcrumbs)
{
    $breadcrumbs->parent('subscribe');
    $breadcrumbs->push('New', route('subscribe.new'));
});
Breadcrumbs::register('subscribe.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('subscribe');
    $breadcrumbs->push('Edit', route('subscribe.edit', $data->id));
});

## COLLECTIONS ##
Breadcrumbs::register('collection', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Collections', route('collection'));
});
Breadcrumbs::register('collection.new', function($breadcrumbs)
{
    $breadcrumbs->parent('collection');
    $breadcrumbs->push('New', route('collection.new'));
});
Breadcrumbs::register('collection.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('collection');
    $breadcrumbs->push('Edit', route('collection.edit', $data->id));
});